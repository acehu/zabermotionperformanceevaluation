import time
import numpy
from zaber_motion import Library
from zaber_motion import Units
from zaber_motion import Measurement
from zaber_motion.ascii import StreamAxisDefinition
from zaber_motion.ascii import StreamAxisType
from zaber_motion.ascii import Connection

Library.enable_device_db_store()



def initiate_stream(stream):
    """
    Initiate streams on all three axes
    :param stream: device's stream
    :return: NA
    """
    stream.setup_live_composite(
        StreamAxisDefinition(1, StreamAxisType.LOCKSTEP),
        StreamAxisDefinition(3, StreamAxisType.PHYSICAL),
        StreamAxisDefinition(4, StreamAxisType.PHYSICAL),
    )


def move_rel_origin(device, x, y, z, speed):
    """
    Move gantry to relative position with streams
    :param speed: velocity of motion is mm/s
    :param z: relative z position in mm (Gantry -X)
    :param y: relative y position in mm (Gantry -Y)
    :param x: relative x position in mm (Gantry -Z)
    :param device: motion controller device
    :return: NA
    """
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    stream.line_relative(Measurement(y, Units.LENGTH_MILLIMETRES),
                         Measurement(-x, Units.LENGTH_MILLIMETRES),
                         Measurement(z, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    stream.disable()


def move_rel(device, x, y, z, speed):
    """
    Move gantry to relative position (relative to current position) with streams
    :param speed: velocity of motion is mm/s
    :param z: relative z position in mm
    :param y: relative y position in mm
    :param x: relative x position in mm
    :param device: motion controller device
    :return: NA
    """
    print("Moving gantry a relative distance of (x1, y3, z4): ({} mm, {} mm, {} mm) with speed {} mm/s".format(x, y, z, speed))
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    stream.line_relative(Measurement(x, Units.LENGTH_MILLIMETRES),
                         Measurement(y, Units.LENGTH_MILLIMETRES),
                         Measurement(z, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    stream.disable()
    print("Gantry moved a relative distance of (x1, y3, z4): ({} mm, {} mm, {} mm)".format(x, y, z))


def move_abs(device, x, y, z, units, speed):
    """
    Move gantry to absolute position with streams
    :param units: mm or microsteps
    :param speed: velocity of motion is mm/s
    :param z: absolute z position in mm
    :param y: absolute y position in mm
    :param x: absolute x position in mm
    :param device: motion controller device
    :return: NA
    """
    if units == 'mm':
        print("Moving gantry to gantry coordinates (x1, y3, z4): ({} mm, {} mm, {} mm) with speed {} mm/s".format(x, y, z, speed))
        time.sleep(1)
        stream = device.get_stream(2)
        initiate_stream(stream)
        set_speed(stream, speed)
        stream.line_absolute(Measurement(x, Units.LENGTH_MILLIMETRES),
                             Measurement(y, Units.LENGTH_MILLIMETRES),
                             Measurement(z, Units.LENGTH_MILLIMETRES))
        stream.wait_until_idle(False)
        stream.disable()
        print("Gantry arrived at gantry coordinates (x1, y3, z4): ({} mm, {} mm, {} mm)".format(x, y, z))
    elif units == 'step' or units == 'steps' or units == 'microstep' or units == 'microsteps' or units == 'uStep':
        print("Moving gantry to gantry coordinates (x1, y3, z4): ({} microsteps, {} microsteps, {} microsteps) with speed {} mm/s".format(x, y, z, speed))
        time.sleep(1)
        stream = device.get_stream(2)
        initiate_stream(stream)
        set_speed(stream, speed)
        stream.line_absolute(Measurement(x, Units.NATIVE),
                             Measurement(y, Units.NATIVE),
                             Measurement(z, Units.NATIVE))
        stream.wait_until_idle(False)
        stream.disable()
        print("Gantry arrived at gantry coordinates (x1, y3, z4): ({} microsteps, {} microsteps, {} microsteps)".format(x, y, z))


def move_abs_wrt_hid_origin(device, x, y, z, speed, sideways):
    """
    Move gantry to absolute position relative to HID Origin with streams
    :param sideways: boolean, whether or not orientation of grasper is sideways
    :param speed: velocity of motion is mm/s
    :param z: absolute z position from origin in mm (Gantry -X)
    :param y: absolute y position from origin in mm (Gantry -Y)
    :param x: absolute x position from origin in mm (Gantry -Z)
    :param device: motion controller device
    :return: NA
    """
    # Origin abs position at 502.4, 543.75, 725.91 (grasper forwards) / 732.11 (grasper sideways)
    ## offset are based on gantry axis
    if sideways:
        # If using Mini45 load cell (wider range, lower res) = fatter by ~4.30 mm
        # x_offset = 723.95+12.63  # 725.91  # 732.11
        # If using Nano43 load cell (lower range, higher res) = skinnier by ~4.30 mm, so move X-axis (wrt HID) closer to HID by +4.30 mm
        x_offset = 723.95+12.63+4.30  # 725.91  # 732.11
    else:
        x_offset = 732.11  # 725.91
    y_offset = 503.046991-10.53  # 484.33  # 488.5
    z_offset = 645.555443-8.07-0.15  # 545.5  # 543.75
    stream = device.get_stream(1)
    initiate_stream(stream)
    set_speed(stream, speed)
    print("Moving gantry to HID coordinates (x, y, z): ({} mm, {} mm, {} mm) with speed {} mm/s".format(x, y, z, speed))
    print("Moving gantry to gantry coordinates (x1, y3, z4): ({} mm, {} mm, {} mm) with speed {} mm/s".format(y_offset + y, x_offset - x, z_offset + z, speed))
    # stream.line_absolute(Measurement(543.75 - z, Units.LENGTH_MILLIMETRES),
    #                      Measurement(y_offset - y, Units.LENGTH_MILLIMETRES),
    #                      Measurement(502.4 - x, Units.LENGTH_MILLIMETRES))
    stream.line_absolute(Measurement(y_offset + y, Units.LENGTH_MILLIMETRES),
                         Measurement(x_offset - x, Units.LENGTH_MILLIMETRES),
                         Measurement(z_offset + z, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    stream.disable()


def transform_origins(point, sideways = True):
    '''
    From HID origin frame to Zaber frame
    '''
    if sideways:
        x_offset = 723.95  # 725.91  # 732.11
    else:
        x_offset = 732.11  # 725.91
    y_offset = 503.046991  # 484.33  # 488.5
    z_offset = 645.555443  # 545.5  # 543.75
    x, y, z = point[0], point[1], point[2]
    transformed_point = (y_offset + y, x_offset - x, z_offset + z)
    return transformed_point


'''
def gantry_trace_arc_abs_origin(device, target_axes_indices, rotation_direction, counter_rotation_direction, center, start, end, speed, cycles=1):
    """
    Have the gantry trace an arc at absolute center and end wrt to HID origin
    """
    print("Begin tracing arc absolute with speed {} mm/s for {} cycles".format(speed, cycles))
    end_copy = end
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    # transform axis
    move_axis = []
    target_axes_indices = numpy.array(target_axes_indices)
    target_axes_indices = numpy.array(
        numpy.dot([[0, 1, 0], [-1, 0, 0], [0, 0, 1]], target_axes_indices.reshape((target_axes_indices.size, 1))))
    for count, value in enumerate(target_axes_indices):
        if value != 0:
            move_axis.append(count)
    start, center, end = transform_origins(start), transform_origins(center), transform_origins(end)
    start_x, start_y = start[move_axis[0]], start[move_axis[1]]
    end_x, end_y = end[move_axis[0]], end[move_axis[1]]
    center_x, center_y = center[move_axis[0]], center[move_axis[1]]
    print(move_axis)
    for i in range(1000):
        try:
            stream.arc_absolute_on(move_axis, counter_rotation_direction,
                                   Measurement(center_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(center_y, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_y, Units.LENGTH_MILLIMETRES))
            stream.arc_absolute_on(move_axis, rotation_direction,
                                   Measurement(center_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(center_y, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_y, Units.LENGTH_MILLIMETRES))
            print ("Success", i)
            break
        except:
            print (i, center_x)
        center_x = center_x - 0.0001
    for i in range(cycles):
        print("Execute cycle {} of {} cycles".format(i+1, cycles))
        stream.arc_absolute_on(move_axis, counter_rotation_direction,
                               Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
                               Measurement(end_x, Units.LENGTH_MILLIMETRES), Measurement(end_y, Units.LENGTH_MILLIMETRES))
        time.sleep(2)
        # move_abs_origin(device, end_copy[0], end_copy[1], end_copy[2], speed, sideways=True)
        # time.sleep(1)
        stream.arc_absolute_on(move_axis, counter_rotation_direction,
                               Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
                               Measurement(start_x, Units.LENGTH_MILLIMETRES), Measurement(start_y, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    print("Finished tracing arc absolute")
'''

def gantry_trace_arc_abs_origin(device, target_axes_indices, rotation_direction, counter_rotation_direction, center, start, end, speed, cycles=1):
    """
    Have the gantry trace an arc at absolute center and end wrt to HID origin
    """

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    print("Begin tracing arc absolute with speed {} mm/s for {} cycles".format(speed, cycles))
    end_copy = end
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    # transform axis
    move_axis = []
    target_axes_indices = numpy.array(target_axes_indices)
    target_axes_indices = numpy.array(
        numpy.dot([[0, 1, 0], [-1, 0, 0], [0, 0, 1]], target_axes_indices.reshape((target_axes_indices.size, 1))))
    for count, value in enumerate(target_axes_indices):
        if value != 0:
            move_axis.append(count)
    start, center, end = transform_origins(start), transform_origins(center), transform_origins(end)
    start_x, start_y = start[move_axis[0]], start[move_axis[1]]
    end_x, end_y = end[move_axis[0]], end[move_axis[1]]
    center_x, center_y = center[move_axis[0]], center[move_axis[1]]
    for i in range(cycles):
        print("Execute cycle {} of {} cycles".format(i+1, cycles))
        stream.arc_absolute_on(move_axis, rotation_direction,
                               Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
                               Measurement(end_x, Units.LENGTH_MILLIMETRES), Measurement(end_y, Units.LENGTH_MILLIMETRES))
        # if axis1.get_position(unit=Units.LENGTH_MILLIMETRES) != end_x:
        #     stream.arc_absolute_on
        print("Half of cycle no. {}".format(i+1))
        time.sleep(2)
        # move_abs_origin(device, end_copy[0], end_copy[1], end_copy[2], speed, sideways=True)
        # time.sleep(1)
        stream.arc_absolute_on(move_axis, counter_rotation_direction,
                               Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
                               Measurement(start_x, Units.LENGTH_MILLIMETRES), Measurement(start_y, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    print("Finished tracing arc absolute")


def gantry_trace_arc_rel_origin(device, target_axes_indices, rotation_direction, counter_rotation_direction, center,
                                start, end, speed, cycles=1):
    """
    Have the gantry trace an arc at center and end relative to current position wrt to HID origin
    """
    print("Begin tracing arc relative with speed {} mm/s for {} cycles".format(speed, cycles))
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    # transform axis
    move_axis = []
    target_axes_indices = numpy.array(target_axes_indices)
    target_axes_indices = numpy.array(
        numpy.dot([[0, 1, 0], [-1, 0, 0], [0, 0, 1]], target_axes_indices.reshape((target_axes_indices.size, 1))))
    for count, value in enumerate(target_axes_indices):
        if value != 0:
            move_axis.append(count)
    start_x, start_y = start[move_axis[0]], start[move_axis[1]]
    end_x, end_y = end[move_axis[0]], end[move_axis[1]]
    center_x, center_y = center[move_axis[0]], center[move_axis[1]]

    ##
    # Keeps trying until it reaches the BADDATA
    # THIS IS JUST FOR FINDING OUT THE TOLERANCE!! DELETE WHEN DONE WITH TESTING
    center_x_original = center_x
    tolerance = [0.00001, 0.00005, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1]
    for tol in tolerance:
        try:
            stream.arc_relative_on(move_axis, rotation_direction,
                                   Measurement(center_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(center_y, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_x, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_y, Units.LENGTH_MILLIMETRES))
            print("Successful")
        except:
            print("BADDATA when tolerance is:", tol)
        center_x = center_x_original + tol
    # THIS IS JUST FOR FINDING OUT THE TOLERANCE!! DELETE WHEN DONE WITH TESTING
    ##

    # for i in range(cycles):
    #     print("Execute cycle {} of {} cycles".format(i+1, cycles))
    #     stream.arc_relative_on(move_axis, rotation_direction,
    #                            Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
    #                            Measurement(end_x, Units.LENGTH_MILLIMETRES), Measurement(end_y, Units.LENGTH_MILLIMETRES))
    #     time.sleep(0.6)
    # stream.arc_relative_on(move_axis, counter_rotation_direction,
    #                        Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
    #                        Measurement(start_x, Units.LENGTH_MILLIMETRES), Measurement(start_y, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    # stream.disable()
    print("Finished tracing arc relative")

'''
def gantry_trace_arc_rel_origin(device, target_axes_indices, rotation_direction, counter_rotation_direction, center, start, end, speed, cycles=1):
    """
    Have the gantry trace an arc at center and end relative to current position wrt to HID origin
    """
    print("Begin tracing arc relative with speed {} mm/s for {} cycles".format(speed, cycles))
    stream = device.get_stream(2)
    initiate_stream(stream)
    set_speed(stream, speed)
    # transform axis
    move_axis = []
    target_axes_indices = numpy.array(target_axes_indices)
    target_axes_indices = numpy.array(
        numpy.dot([[0, 1, 0], [-1, 0, 0], [0, 0, 1]], target_axes_indices.reshape((target_axes_indices.size, 1))))
    for count, value in enumerate(target_axes_indices):
        if value != 0:
            move_axis.append(count)
    start_x, start_y = start[move_axis[0]], start[move_axis[1]]
    end_x, end_y = end[move_axis[0]], end[move_axis[1]]
    center_x, center_y = center[move_axis[0]], center[move_axis[1]]
    
    ##
    # Keeps trying until it reaches the BADDATA
    # THIS IS JUST FOR FINDING OUT THE TOLERANCE!! DELETE WHEN DONE WITH TESTING
    center_x_original = center_x
    tolerance = [0.00001,0.00005,0.0001,0.0005,0.001,0.005,0.01,0.05,0.1]
    for tol in tolerance:
        try:
            stream.arc_relative_on(move_axis, rotation_direction,
                                   Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
                                   Measurement(end_x, Units.LENGTH_MILLIMETRES), Measurement(end_y, Units.LENGTH_MILLIMETRES))
            print ("Successful")
        except:
            print ("BADDATA when tolerance is:", tol)
        center_x = center_x_original + tol
    # THIS IS JUST FOR FINDING OUT THE TOLERANCE!! DELETE WHEN DONE WITH TESTING
    ##

    # for i in range(cycles):
    #     print("Execute cycle {} of {} cycles".format(i+1, cycles))
    #     stream.arc_relative_on(move_axis, rotation_direction,
    #                            Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
    #                            Measurement(end_x, Units.LENGTH_MILLIMETRES), Measurement(end_y, Units.LENGTH_MILLIMETRES))
    #     time.sleep(0.6)
        # stream.arc_relative_on(move_axis, counter_rotation_direction,
        #                        Measurement(center_x, Units.LENGTH_MILLIMETRES),Measurement(center_y, Units.LENGTH_MILLIMETRES),
        #                        Measurement(start_x, Units.LENGTH_MILLIMETRES), Measurement(start_y, Units.LENGTH_MILLIMETRES))
    stream.wait_until_idle(False)
    # stream.disable()
    print("Finished tracing arc relative")
'''


def set_speed(stream, speed):
    """

    :param speed: velocity of motion is mm/s
    :param stream: device's stream
    :return: NA
    """
    stream.set_max_speed(speed, unit=Units.VELOCITY_MILLIMETRES_PER_SECOND)

def set_speed_linear(device, axis_num, speed):
    axis = device.get_axis(axis_num)
    axis.settings.set(setting="maxspeed", value=speed, unit=Units.VELOCITY_MILLIMETRES_PER_SECOND)
    print("Axis {} speed set to: axis.settings.get maxspeed = {} mm/s".format(axis_num, axis.settings.get(setting="maxspeed", unit=Units.VELOCITY_MILLIMETRES_PER_SECOND)))

def set_speed_rotary(device, speed):
    """

    :param speed: velocity of motion is mm/s
    :param stream: device's stream
    :return: NA
    """
    axis_rotary = device.get_axis(1)
    axis_rotary.settings.set(setting="maxspeed", value=speed, unit=Units.ANGULAR_VELOCITY_DEGREES_PER_SECOND)
    print("axis_rotary.settings.get maxspeed = {}°/s".format(axis_rotary.settings.get(setting="maxspeed", unit=Units.ANGULAR_VELOCITY_DEGREES_PER_SECOND)))


def wait_until_idle_linear(device, wait=True):
    """
    Wait for device to finish all actions (non-stream command)
    :param device: motion controller device
    :return: NA
    """
    device.get_lockstep(1).wait_until_idle(wait)
    device.get_axis(3).wait_until_idle(wait)
    device.get_axis(4).wait_until_idle(wait)

def wait_until_idle_rotary(device, wait=True):
    """
    Wait for device to finish all actions (non-stream command)
    :param device: motion controller device
    :return: NA
    """
    device.get_axis(1).wait_until_idle(wait)


def do_x_sin(device, amplitude, frequency, cycles):
    """
    Moves the X axis in a sin wave motion with the specified parameters

    :param device: motion controller device
    :param amplitude: amplitude of the desired sin wave (mm)
    :param frequency: frequency of the desired sin wave (Hz)
    :param cycles: number of cycles of the sine wave (int)
    :return: NA
    """
    
    axis = device.get_axis(3)
    
    steps_per_mm = 1 / 0.001984375
    steps_amplitude = round(amplitude * steps_per_mm)
    period = round(1 / frequency * 1000)
    cycles = round(cycles)

    # Calculates the avg velocity of the sin motion (mm/s)
    sin_velocity = round(1 * amplitude / (period / 1000))  # originally: 4 * amplitude ...

    # Accounts for sin motion offset
    move_rel(device, 0, -amplitude, 0, sin_velocity)

    # Sin motion
    print("Executing X-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    axis.generic_command("move sin " + str(steps_amplitude) + " " + str(period) + " " + str(cycles))
    wait_until_idle_linear(device, False)

    # Accounts for sin motion offset
    move_rel(device, 0, amplitude, 0, sin_velocity)


def do_yy_sin(device, amplitude, frequency, cycles):
    """
    Moves the Y axis in a sin wave motion with the specified parameters

    :param device: motion controller device
    :param amplitude: amplitude of the desired sin wave (mm)
    :param frequency: frequency of the desired sin wave (Hz)
    :param cycles: number of cycles of the sine wave (int)
    :return: NA
    """
    steps_per_mm = 1 / 0.001984375
    steps_amplitude = round(amplitude * steps_per_mm)
    period = round(1 / frequency * 1000)
    cycles = round(cycles)

    # Calculates the avg velocity of the sin motion (mm/s)
    sin_velocity = round(1 * amplitude / (period / 1000))  # originally: 4 * amplitude ...

    # Accounts for sin motion offset
    move_rel(device, -amplitude, 0, 0, sin_velocity)

    # Sin motion
    print("Executing Y-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    device.generic_command("lockstep 1 move sin " + str(steps_amplitude) + " " + str(period) + " " + str(cycles))
    wait_until_idle_linear(device, False)

    # Accounts for sin motion offset
    move_rel(device, amplitude, 0, 0, sin_velocity)


def do_z_sin(device, amplitude, frequency, cycles):
    """
    Moves the Z axis in a sin wave motion with the specified parameter

    :param device: motion controller device
    :param amplitude: amplitude of the desired sin wave (mm)
    :param frequency: frequency of the desired sin wave (Hz)
    :param cycles: number of cycles of the sine wave (int)
    :return: NA
    """
    
    axis = device.get_axis(4)
    
    steps_per_mm = 1 / 0.001984375
    steps_amplitude = round(amplitude * steps_per_mm)
    period = round(1 / frequency * 1000)
    cycles = round(cycles)

    # Calculates the avg velocity of the sin motion (mm/s)
    sin_velocity = round(1 * amplitude / (period / 1000))  # originally: 4 * amplitude ...

    # Accounts for sin motion offset
    move_rel(device, 0, 0, -amplitude, sin_velocity)

    # Sin motion
    print("Executing Z-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    axis.generic_command("move sin " + str(steps_amplitude) + " " + str(period) + " " + str(cycles))
    wait_until_idle_linear(device, False)

    # Accounts for sin motion offset
    move_rel(device, 0, 0, amplitude, sin_velocity)


def do_interaction_force(device, amplitude, frequency, cycles):
    """
    Performs sin motion interaction force test in each of the X, Y, Z axes
    :param device: motion controller device
    :param amplitude: amplitude of the desired sin wave (mm)
    :param frequency: frequency of the desired sin wave (Hz)
    :param cycles: number of cycles of the sine wave (int)
    :return: NA
    """

    # X-stage interaction force motion
    time.sleep(1)
    print("Executing X-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    time.sleep(2)
    set_analog_output(device, 1)
    do_x_sin(device, amplitude, frequency, cycles)
    wait_until_idle_linear(device, False)

    # pause(device, 10)
    time.sleep(3)

    # Y-stage interaction force motion
    time.sleep(1)
    print("Executing Y-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    time.sleep(2)
    set_analog_output(device, 2)
    do_yy_sin(device, amplitude, frequency, cycles)
    wait_until_idle_linear(device, False)

    # pause(device, 10)
    time.sleep(3)

    # Z-stage interaction force motion
    time.sleep(1)
    print("Executing Z-axis sinusoidal motion with {} mm amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    time.sleep(2)
    set_analog_output(device, 3)
    do_z_sin(device, amplitude, frequency, cycles)
    wait_until_idle_linear(device, False)

    # Reset analog output to 0
    set_analog_output(device, 0)


def do_force_to_move(device, trajectory='trapezoid', distance=0, speed=0, cycles=0):
    """
    Moves each axis at specified speed in one direction and back.

    :param speed: speed of axis motion (mm/s)
    :param distance: distance of axis motion (mm)
    :param device: motion controller device
    :return: NA
    """

    cycles = round(cycles)

    if trajectory == 'trapezoid':

        move_rel(device, 0, -distance / 2, 0, speed)

        # X-stage force to move motion
        for i in range(cycles):
            print("Executing cycle {} of {} cycles along X-axis".format(i+1, cycles))
            time.sleep(1)
            print("Moving X-axis stage {} mm at {} mm/s in +X direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, 0, distance, 0, speed)
            time.sleep(1)
            print("Moving X-axis stage {} mm at {} mm/s in -X direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, 0, -distance, 0, speed)

        time.sleep(3)
        move_rel(device, -distance/2, distance/2, 0, speed)

        # Y-stage force to move motion
        for j in range(cycles):
            print("Executing cycle {} of {} cycles along Y-axis".format(j+1, cycles))
            time.sleep(1)
            print("Moving Y-axis stage {} mm at {} mm/s in +Y direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, distance, 0, 0, speed)
            time.sleep(1)
            print("Moving Y-axis stage {} mm at {} mm/s in -Y direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, -distance, 0, 0, speed)

        time.sleep(3)
        move_rel(device, distance/2, 0, -distance/2, speed)

        # Z-stage force to move motion
        for k in range(cycles):
            print("Executing cycle {} of {} cycles along Z-axis".format(k+1, cycles))
            time.sleep(1)
            print("Moving Z-axis stage {} mm at {} mm/s in +Z direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, 0, 0, distance, speed)
            time.sleep(1)
            print("Moving Z-axis stage {} mm at {} mm/s in -Z direction".format(distance, speed))
            time.sleep(2)
            move_rel(device, 0, 0, -distance, speed)

        time.sleep(3)
        move_rel(device, 0, 0, distance/2, speed)

    elif trajectory == 'triangle':

        move_rel(device, 0, -distance / 2, 0, speed)

        # X-stage force to move motion
        for i in range(cycles):
            print("Executing cycle {} of {} cycles along X-axis".format(i+1, cycles))
            print("Moving X-axis stage {} mm at {} mm/s in +X direction".format(distance, speed))
            move_rel(device, 0, distance, 0, speed)
            print("Moving X-axis stage {} mm at {} mm/s in -X direction".format(distance, speed))
            move_rel(device, 0, -distance, 0, speed)

        time.sleep(3)
        move_rel(device, -distance/2, distance/2, 0, speed)

        # Y-stage force to move motion
        for j in range(cycles):
            print("Executing cycle {} of {} cycles along Y-axis".format(j+1, cycles))
            print("Moving Y-axis stage {} mm at {} mm/s in +Y direction".format(distance, speed))
            move_rel(device, distance, 0, 0, speed)
            print("Moving Y-axis stage {} mm at {} mm/s in -Y direction".format(distance, speed))
            move_rel(device, -distance, 0, 0, speed)

        time.sleep(3)
        move_rel(device, distance/2, 0, -distance/2, speed)

        # Z-stage force to move motion
        for k in range(cycles):
            print("Executing cycle {} of {} cycles along Z-axis".format(k+1, cycles))
            print("Moving Z-axis stage {} mm at {} mm/s in +Z direction".format(distance, speed))
            move_rel(device, 0, 0, distance, speed)
            print("Moving Z-axis stage {} mm at {} mm/s in -Z direction".format(distance, speed))
            move_rel(device, 0, 0, -distance, speed)

        time.sleep(3)
        move_rel(device, 0, 0, distance/2, speed)


def do_haptics(device, distance, speed):
    """
    Moves each axis at specified speed in one direction and returns to original position.

    :param speed: speed of axis motion (mm/s)
    :param distance: distance of axis motion (mm)
    :param device: motion controller device
    :return: NA
    """

    # X axis haptic motion
    move_rel(device, distance, 0, 0, speed)
    move_rel(device, -distance, 0, 0, 75)

    # Y axis haptic motion
    move_rel(device, 0, distance, 0, speed)
    move_rel(device, 0, -distance, 0, 75)

    # Z axis haptic motion
    move_rel(device, 0, 0, distance, speed)
    move_rel(device, 0, 0, -distance, 75)


def rotate_to_abs(device, angle, speed):
    """
    Moves rotational axis to specified absolute angle.

    :param angle: desired absolute position (degrees)
    :param device: motion controller device
    :return: NA
    """
    axis = device.get_axis(1)
    set_speed_rotary(device, speed)
    print("Moving rotary-stage to angle {}° with speed {}°/s".format(angle, speed))
    axis.move_absolute(angle, Units.ANGLE_DEGREES, True)


def rotate_to_rel(device, angle, speed):
    """
    Moves rotational axis to specified relative angle.

    :param angle: desired absolute position (degrees)
    :param device: motion controller device
    :return: NA
    """
    axis = device.get_axis(1)
    set_speed_rotary(device, speed)
    print("Moving rotary-stage a relative angle of {}°  with speed {}°/s".format(angle, speed))
    axis.move_relative(angle, Units.ANGLE_DEGREES, True)


def rotate_sin(device, amplitude, frequency, cycles):
    """
    Moves the rotational axis in a sin wave motion with the specified parameters

    :param device: motion controller device
    :param amplitude: amplitude of the desired sin wave (mm)
    :param frequency: frequency of the desired sin wave (Hz)
    :param cycles: number of cycles of the sine wave (int)
    :return: NA
    """
    
    axis = device.get_axis(1)

    amplitude = float(amplitude)
    frequency = float(frequency)
    cycles = int(cycles)
    
    steps_per_deg = 1 / 0.0009375
    steps_amplitude = round(amplitude * steps_per_deg, 2)
    period = round(1 / frequency * 1000)
    cycles = round(cycles)
    
    # For sine amplitudes < 30 deg, the Zaber rotary stage has a resolution issue, so convert degree to micro-step.
    if amplitude < 30 or amplitude < 30.0:
        # steps_amplitude = AxisSettings.convert_to_native_units(setting="pos", value=amplitude, unit=Units.NATIVE)
        steps_amplitude = int(1066.7 * amplitude - 0.1333)
        print("For sine amplitudes < 30 deg, the Zaber rotary stage has a resolution issue.\nConverting degree to micro-step: from {}° to {} NATIVE. ".format(amplitude, steps_amplitude))

    # Accounts for sin motion offset
    print("Accounting for sin motion offset, pre-sine")
    axis.move_relative(-amplitude, Units.ANGLE_DEGREES, True)

    # Sin motion
    print("Executing Rotary-axis sinusoidal motion with {}° amplitude and {} Hz frequency for {} cycles".format(amplitude, frequency, cycles))
    axis.generic_command("move sin " + str(steps_amplitude) + " " + str(period) + " " + str(cycles))
    wait_until_idle_rotary(device, False)

    # Accounts for sin motion offset
    print("Accounting for sin motion offset, post-sine")
    axis.move_relative(amplitude, Units.ANGLE_DEGREES, True)


def do_torque_to_move(device, trajectory='trapezoid', start_point='end', angle=0, speed=0, cycles=0):
    """
    Moves the rotational axis at specified speed in one direction and back

    :param speed: speed of axis motion (mm/s)
    :param distance: distance of axis motion (mm)
    :param device: motion controller device
    :return: NA
    """

    angle = float(angle)
    speed = float(speed)
    cycles = int(cycles)

    cycles = round(cycles)

    if trajectory == 'trapezoid':

        if start_point == 'middle':

            rotate_to_rel(device, -angle/2, speed)

            # Rotary-stage torque to move motion
            for l in range(cycles):
                print("Executing cycle {} of {} cycles for rotary-axis".format(l+1, cycles))
                time.sleep(1)
                print("Moving rotary-axis stage {}° at {}°/s in + direction".format(angle, speed))
                time.sleep(2)
                rotate_to_rel(device, angle, speed)
                time.sleep(1)
                print("Moving rotary-axis stage {}° at {}°/s in - direction".format(angle, speed))
                time.sleep(2)
                rotate_to_rel(device, -angle, speed)

            rotate_to_rel(device, angle/2, speed)

        elif start_point == 'end':

            # Rotary-stage torque to move motion
            for l in range(cycles):
                print("Executing cycle {} of {} cycles for rotary-axis".format(l+1, cycles))
                time.sleep(1)
                print("Moving rotary-axis stage {}° at {}°/s in + direction".format(angle, speed))
                time.sleep(2)
                rotate_to_rel(device, angle, speed)
                time.sleep(1)
                print("Moving rotary-axis stage {}° at {}°/s in - direction".format(angle, speed))
                time.sleep(2)
                rotate_to_rel(device, -angle, speed)

    elif trajectory == 'triangle':

        if start_point == 'middle':

            rotate_to_rel(device, -angle/2, speed)

            # Rotary-stage torque to move motion
            for l in range(cycles):
                print("Executing cycle {} of {} cycles for rotary-axis".format(l+1, cycles))
                print("Moving rotary-axis stage {}° at {}°/s in + direction".format(angle, speed))
                rotate_to_rel(device, angle, speed)
                print("Moving rotary-axis stage {}° at {}°/s in - direction".format(angle, speed))
                rotate_to_rel(device, -angle, speed)

            rotate_to_rel(device, angle/2, speed)

        elif start_point == 'end':

            for l in range(cycles):
                print("Executing cycle {} of {} cycles for rotary-axis".format(l+1, cycles))
                print("Moving rotary-axis stage {}° at {}°/s in + direction".format(angle, speed))
                rotate_to_rel(device, angle, speed)
                print("Moving rotary-axis stage {}° at {}°/s in - direction".format(angle, speed))
                rotate_to_rel(device, -angle, speed)


def home_all_axes_linear(device):
    """
    Home Z, Y, and X axes
    :param com_port:
    :param device: motion controller device
    :return: NA
    """
    print("\n *** Start Homing all 4 Zaber stages! ***")

    # with Connection.open_serial_port(com_port) as connection:
    #     device_list = connection.detect_devices()
    #     print("Found {} devices".format(len(device_list)))
    #     device = device_list[0]
    #     print("Number of axes on this device: {}".format(device.axis_count))

    time.sleep(1)

    device.get_axis(4).home()
    device.get_axis(3).home()
    device.get_lockstep(1).home()

    device.get_axis(4).move_relative(1, Units.LENGTH_MILLIMETRES, True)
    device.get_axis(3).move_relative(1, Units.LENGTH_MILLIMETRES, True)
    device.get_lockstep(1).move_relative(1, Units.LENGTH_MILLIMETRES, True)

    print("*** All 4 Zaber stages homed! *** \n")


def home_all_axes_rotary(device):
    """
    Home rotary stage
    :param com_port:
    :param device: motion controller device
    :return: NA
    """
    print("\n *** Start Homing Rotary Zaber stages! ***")

    # with Connection.open_serial_port(com_port) as connection:
    #     device_list = connection.detect_devices()
    #     print("Found {} devices".format(len(device_list)))
    #     device = device_list[0]
    #     print("Number of axes on this device: {}".format(device.axis_count))

    time.sleep(1)

    device.get_axis(1).home()
    if device.axis_count == 2:
        prompt_home_second_rotary_stage = input("Would you like to home the 2nd rotary stage as well? Input 'Y' or 'N'\n")
        if prompt_home_second_rotary_stage == 'Y':
            device.get_axis(2).home()

    print("*** All Rotary Zaber stages homed! *** \n")


def pause(device, time):
    """

    :param device: motion controller device
    :param time: time to wait (sec)
    :return: NA
    """
    stream = device.get_stream(2)
    initiate_stream(stream)
    stream.wait(time, Units.TIME_SECONDS)
    stream.disable()


def get_max_current_linear(device):

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    # Read max current (can only read max current)
    print("\nMAX CURRENTS:")
    print("axis1.settings.get driver.current.max = {} DC_ELECTRIC_CURRENT_AMPERES".format(axis1.settings.get(setting="driver.current.max", unit=Units.DC_ELECTRIC_CURRENT_AMPERES)))
    print("axis2.settings.get driver.current.max = {} DC_ELECTRIC_CURRENT_AMPERES".format(axis2.settings.get(setting="driver.current.max", unit=Units.DC_ELECTRIC_CURRENT_AMPERES)))
    print("axis3.settings.get driver.current.max = {} DC_ELECTRIC_CURRENT_AMPERES".format(axis3.settings.get(setting="driver.current.max", unit=Units.DC_ELECTRIC_CURRENT_AMPERES)))
    print("axis4.settings.get driver.current.max = {} DC_ELECTRIC_CURRENT_AMPERES".format(axis4.settings.get(setting="driver.current.max", unit=Units.DC_ELECTRIC_CURRENT_AMPERES)))

def get_run_current_linear(device):

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    # Read run current
    print("\nRUN CURRENTS SET TO:")
    print("axis1.settings.get driver.current.run = {} AC_ELECTRIC_CURRENT_AMPERES_PEAK".format(axis1.settings.get(setting="driver.current.run", unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)))
    print("axis2.settings.get driver.current.run = {} AC_ELECTRIC_CURRENT_AMPERES_PEAK".format(axis2.settings.get(setting="driver.current.run", unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)))
    print("axis3.settings.get driver.current.run = {} AC_ELECTRIC_CURRENT_AMPERES_PEAK".format(axis3.settings.get(setting="driver.current.run", unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)))
    print("axis4.settings.get driver.current.run = {} AC_ELECTRIC_CURRENT_AMPERES_PEAK".format(axis4.settings.get(setting="driver.current.run", unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)))

def get_hold_current_linear(device):

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    # Read hold current
    print("\nHOLD CURRENTS SET TO:")
    print("axis1.settings.get driver.current.hold = {} NATIVE".format(axis1.settings.get(setting="driver.current.hold", unit=Units.NATIVE)))
    print("axis2.settings.get driver.current.hold = {} NATIVE".format(axis2.settings.get(setting="driver.current.hold", unit=Units.NATIVE)))
    print("axis3.settings.get driver.current.hold = {} NATIVE".format(axis3.settings.get(setting="driver.current.hold", unit=Units.NATIVE)))
    print("axis4.settings.get driver.current.hold = {} NATIVE".format(axis4.settings.get(setting="driver.current.hold", unit=Units.NATIVE)))


def set_run_current_linear(device, run_current_axis1=1.23, run_current_axis3=1.23, run_current_axis4=1.80):

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    # Set run current
    axis1.settings.set(setting="driver.current.run", value=run_current_axis1, unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
    axis2.settings.set(setting="driver.current.run", value=run_current_axis1, unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
    axis3.settings.set(setting="driver.current.run", value=run_current_axis3, unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
    axis4.settings.set(setting="driver.current.run", value=run_current_axis4, unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK. Set z-axis to 90 NATIVE to counter gravity

def set_hold_current_linear(device, hold_current_axis1=80.0, hold_current_axis3=80.0, hold_current_axis4=120.0):

    axis1 = device.get_axis(1)
    axis2 = device.get_axis(2)
    axis3 = device.get_axis(3)
    axis4 = device.get_axis(4)

    # Set hold current
    axis1.settings.set(setting="driver.current.hold", value=hold_current_axis1, unit=Units.NATIVE)  # For x1, x2, y3 stages: default = 80 NATIVE. Set to 0 NATIVE to backdrive.
    axis2.settings.set(setting="driver.current.hold", value=hold_current_axis1, unit=Units.NATIVE)  # For x1, x2, y3 stages: default = 80 NATIVE. Set to 0 NATIVE to backdrive.
    axis3.settings.set(setting="driver.current.hold", value=hold_current_axis3, unit=Units.NATIVE)  # For x1, x2, y3 stages: default = 80 NATIVE. Set to 0 NATIVE to backdrive.
    axis4.settings.set(setting="driver.current.hold", value=hold_current_axis4, unit=Units.NATIVE)  # For z4-stage: default = 120 NATIVE. Set to 0 NATIVE to backdrive.


def get_max_current_rotary(device):

    axis_rotary = device.get_axis(1)

    # Read max current (can only read max current)
    print("\nMAX CURRENT:")
    print("axis_rotary.settings.get driver.current.max = {} DC_ELECTRIC_CURRENT_AMPERES".format(axis_rotary.settings.get(setting="driver.current.max", unit=Units.DC_ELECTRIC_CURRENT_AMPERES)))

def get_run_current_rotary(device):

    axis_rotary = device.get_axis(1)

    # Read run current
    print("\nRUN CURRENT SET TO:")
    print("axis_rotary.settings.get driver.current.run = {} AC_ELECTRIC_CURRENT_AMPERES_PEAK".format(axis_rotary.settings.get(setting="driver.current.run", unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)))

def get_hold_current_rotary(device):

    axis_rotary = device.get_axis(1)

    # Read hold current
    print("\nHOLD CURRENT SET TO:")
    print("axis_rotary.settings.get driver.current.hold = {} NATIVE".format(axis_rotary.settings.get(setting="driver.current.hold", unit=Units.NATIVE)))


def set_run_current_rotary(device, run_current_rotary=0.56):

    axis_rotary = device.get_axis(1)

    # Set run current
    axis_rotary.settings.set(setting="driver.current.run", value=run_current_rotary, unit=Units.AC_ELECTRIC_CURRENT_AMPERES_PEAK)  # For rotary stages: Max (default) = 50 NATIVE or 1.4 AC_PEAK / Min = 20 NATIVE or 0.56 AC_PEAK

def set_hold_current_rotary(device, hold_current_rotary=25.0):

    axis_rotary = device.get_axis(1)

    # Set hold current
    axis_rotary.settings.set(setting="driver.current.hold", value=hold_current_rotary, unit=Units.NATIVE)  # For rotary stages: default = 25 NATIVE. Set to 0 NATIVE to backdrive.


def warnings_print(device):
    print("WARNINGS: \n{}\n".format(device.warnings.get_flags()))

def warnings_clear(device):
    print("WARNINGS: \n{}\n".format(device.warnings.get_flags()))
    print("CLEARING WARNINGS \n")
    device.warnings.clear_flags()
    # device.get_axis(1).settings.set(setting="driver.enable", value=1)
    # axis1.settings.set(setting="driver.enable")
    # axis2.settings.set(setting="driver.enable")
    # axis3.settings.set(setting="driver.enable")
    # axis4.settings.set(setting="driver.enable")
    if device.warnings.get_flags() == "{'FH'}" or device.warnings.get_flags() == "{'WL', 'NI'}":
        print("WARNINGS FAILED TO CLEAR\n")
    else:
        print("WARNINGS CLEARED!\n")
    print("WARNINGS: \n{}\n".format(device.warnings.get_flags()))


def stop_all_stages(com_port):

    print("Stopping stages!")

    with Connection.open_serial_port(com_port) as connection:
        device_list = connection.detect_devices()
        print("Found {} devices".format(len(device_list)))
        device = device_list[0]
        print("Number of axes on this device: {}".format(device.axis_count))

        if device.axis_count == 4:

            axis1 = device.get_axis(1)
            axis2 = device.get_axis(2)
            axis3 = device.get_axis(3)
            axis4 = device.get_axis(4)

            device.get_lockstep(1).stop(wait_until_idle=False)
            # device.get_lockstep(1).stop(wait_until_idle=False)
            device.get_axis(3).stop(wait_until_idle=False)
            # device.get_axis(3).stop(wait_until_idle=False)
            device.get_axis(4).stop(wait_until_idle=False)
            # device.get_axis(4).stop(wait_until_idle=False)

            # device.get_lockstep(1).settings.set(setting="driver.disable")
            # device.get_axis(3).settings.set(setting="driver.disable")
            # device.get_axis(4).settings.set(setting="driver.disable")

            # device.get_lockstep(1).settings.set(setting="driver.disable")  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
            # device.get_lockstep(1).settings.set(setting="driver.disable")
            # device.get_axis(3).settings.set(setting="driver.disable")  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
            # device.get_axis(4).settings.set(setting="driver.disable")  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK
            # axis4.settings.set(setting="driver.disable")  # For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK. Set z-axis to 90 NATIVE to counter gravity

        if device.axis_count == 1 or device.axis_count == 2:

            axis_rotary = device.get_axis(1)

            device.get_axis(1).stop(wait_until_idle=False)
            # device.get_axis(1).settings.set(setting="driver.disable")

    print("Stages stopped!")


def encoder(com_port):
    with Connection.open_serial_port(com_port) as connection:
        device_list = connection.detect_devices()
        # print("Found {} devices".format(len(device_list)))
        device = device_list[0]
        # print("Number of axes on this device: {}".format(device.axis_count))

        if device.axis_count == 4:

            axis1 = device.get_axis(1)
            axis2 = device.get_axis(2)
            axis3 = device.get_axis(3)
            axis4 = device.get_axis(4)

            axis1_encoder = axis1.get_position(Units.LENGTH_MILLIMETRES)  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            axis2_encoder = axis2.get_position(Units.LENGTH_MILLIMETRES)  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            axis3_encoder = axis3.get_position(Units.LENGTH_MILLIMETRES)  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            axis4_encoder = axis4.get_position(Units.LENGTH_MILLIMETRES)  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)

            return axis1_encoder, axis2_encoder, axis3_encoder, axis4_encoder


def set_analog_output(device, voltage):
    # Set first analog channel output
    device.io.set_analog_output(1, voltage)  # 0-10 V Range, 2.5 mV resolution


def print_analog_output(device):
    # Display current analog output
    print(device.io.get_analog_output(1))
import configparser
import Constants
from Helper import system_check


class Gantry_config():
    def __init__(self, file):
        self.config = configparser.ConfigParser()
        self.config.read(file)
        self.initialize_reading_config()
        self.type_linear_movement = True
        self.movement = None
        self.notes = str(self.config['zaber_config']['notes'])
        self.usb_com_port = str(self.config['zaber_config']['usb_com_port'])
        self.gantry_sideways_clamp = self.config['zaber_config'].getboolean('gantry_sideways_clamp')
        self.action_to_execute = str(self.config['zaber_config']['action_to_execute'])
        self.extract_type(self.action_to_execute)
        self.gantry_X_mm_steps = float(self.config['zaber_config']['gantry_X_mm_steps'])
        self.gantry_Y_mm_steps = float(self.config['zaber_config']['gantry_Y_mm_steps'])
        self.gantry_Z_mm_steps = float(self.config['zaber_config']['gantry_Z_mm_steps'])
        self.gantry_units = str(self.config['zaber_config']['gantry_units'])
        self.gantry_speed_mm_s = float(self.config['zaber_config']['gantry_speed_mm_s'])
        self.rotary_angle_deg = float(self.config['zaber_config']['rotary_angle_deg'])
        self.rotary_speed_deg_s = float(self.config['zaber_config']['rotary_speed_deg_s'])
        self.arc_speed_mm_s = float(self.config['zaber_config']['arc_speed_mm_s'])
        self.arc_cycles = int(self.config['zaber_config']['arc_cycles'])
        self.interaction_amplitude_mm_deg = self.config['zaber_config']['interaction_amplitude_mm_deg']
        self.interaction_frequency_hz = self.config['zaber_config']['interaction_frequency_hz']
        self.interaction_cycles = self.config['zaber_config']['interaction_cycles']
        self.friction_motion_profile = self.config['zaber_config']['friction_motion_profile']
        self.friction_start_point = self.config['zaber_config']['friction_start_point']
        self.friction_PeakToPeak_distance_mm_deg = self.config['zaber_config']['friction_PeakToPeak_distance_mm_deg']
        self.friction_speed_mm_deg_sec = self.config['zaber_config']['friction_speed_mm_deg_sec']
        self.friction_cycles = self.config['zaber_config']['friction_cycles']
        self.zaber_config = self.initialize_zaber_config(self.movement)
        self.run_time = self.zaber_config.calculate_run_time()
        if self.config.has_option('readings_config', 'run_time'):
            self.run_time = int(self.config['readings_config']['run_time'])
        self.number_of_samples = int(self.sampling_freq * self.run_time)
        self.check_config()

    def check_config(self):
        system_check(checkConfig(self.zaber_config))

    def initialize_zaber_config(self, type):
        """Initialize Zaber Config"""
        if type == "arc":
            return arc_config(self.type_linear_movement, self.config)
        elif type == "interaction":
            return interaction_config(self.type_linear_movement, self.config)
        elif type == "linear_velocity":
            return force_to_start_config(self.type_linear_movement, self.config)
        else:
            return general_config(self.type_linear_movement, self.config)

    def initialize_reading_config(self):
        """Initialize Reading Config"""
        self.load_cell_sn = int(self.config['readings_config']['load_cell_sn'])
        self.sampling_freq = int(self.config['readings_config']['sampling_freq'])
        self.reuse_offsets = self.config['readings_config'].getboolean('reuse_offsets')
        self.use_lowPass_filter = self.config['readings_config'].getboolean('use_lowPass_filter')
        self.filter_cutoff_freq = int(self.config['readings_config']['filter_cutoff_freq'])
        self.filter = None
        if self.use_lowPass_filter:
            self.filter = Constants.lowPass_filter
        self.running_mean_window_size = int(self.config['readings_config']['running_mean_window_size'])
        self.run_time = 0

    def extract_type(self, action_to_execute):
        """Extract and find the action type"""
        if "rotary_" in action_to_execute:
            self.type_linear_movement = False
        if "_arc_" in action_to_execute:
            self.movement = "arc"
            self.type_linear_movement = False
        elif "_interaction_" in action_to_execute:
            self.movement = "interaction"
        elif "to_start" in action_to_execute:
            self.movement = "linear_velocity"
        else:
            self.movement = "move_general"


class general_config():
    """General Movement"""

    def __init__(self, type_linear_movement, config):
        self.type_linear_movement = type_linear_movement
        if type_linear_movement:
            self.gantry_sideways_clamp = config['zaber_config'].getboolean('gantry_sideways_clamp')
            self.gantry_X_mm_steps = float(config['zaber_config']['gantry_X_mm_steps'])
            self.gantry_Y_mm_steps = float(config['zaber_config']['gantry_Y_mm_steps'])
            self.gantry_Z_mm_steps = float(config['zaber_config']['gantry_Z_mm_steps'])
            self.gantry_units = str(config['zaber_config']['gantry_units'])
            self.gantry_speed_mm_s = float(config['zaber_config']['gantry_speed_mm_s'])
        else:
            self.rotary_angle_deg = float(config['zaber_config']['rotary_angle_deg'])
            self.rotary_speed_deg_s = float(config['zaber_config']['rotary_speed_deg_s'])

    def calculate_run_time(self):
        return 15

    def customize_position(self, x, y, z):
        '''change position'''
        self.gantry_X_mm_steps = x
        self.gantry_Y_mm_steps = y
        self.gantry_Z_mm_steps = z


class arc_config():
    """arc movement for joint data"""

    def __init__(self, type_linear_movement, config):
        self.arc_speed_mm_s = float(config['zaber_config']['arc_speed_mm_s'])
        self.arc_cycles = int(config['zaber_config']['arc_cycles'])
        self.test_system, self.point_to_test, self.x, self.y, self.z = read_points('point_lists.ini', type_linear_movement)
        self.x_start, self.y_start, self.z_start = self.x[0], self.y[0], self.z[0]
        self.x_end, self.y_end, self.z_end = self.x[1], self.y[1], self.z[1]

    def customize_position(self, x, y, z):
        '''change position'''
        self.gantry_X_mm_steps = x
        self.gantry_Y_mm_steps = y
        self.gantry_Z_mm_steps = z

    def calculate_run_time(self):
        return 50


class interaction_config():
    """ interaction sine motions """

    def __init__(self, type_linear_movement, config):
        self.type_linear_movement = type_linear_movement
        self.interaction_amplitude_mm_deg = float(config['zaber_config']['interaction_amplitude_mm_deg'])
        self.interaction_frequency_hz = float(config['zaber_config']['interaction_frequency_hz'])
        self.interaction_cycles = float(config['zaber_config']['interaction_cycles'])

    def calculate_run_time(self):
        if self.type_linear_movement:
            run_time = (((1 / self.interaction_frequency_hz) * self.interaction_cycles) * 3) + 30
        else:
            run_time = ((1 / self.interaction_frequency_hz) * self.interaction_cycles * 1) + 15
        return run_time

    def customize_position(self, x, y, z):
        '''change position'''
        self.gantry_X_mm_steps = x
        self.gantry_Y_mm_steps = y
        self.gantry_Z_mm_steps = z

class force_to_start_config():
    """ force to start movement """

    def __init__(self, type_linear_movement, config):
        self.type_linear_movement = type_linear_movement
        self.friction_motion_profile = str(config['zaber_config']['friction_motion_profile'])
        self.friction_start_point = str(config['zaber_config']['friction_start_point'])
        self.friction_PeakToPeak_distance_mm_deg = float(config['zaber_config']['friction_PeakToPeak_distance_mm_deg'])
        self.friction_speed_mm_deg_sec = float(config['zaber_config']['friction_speed_mm_deg_sec'])
        self.friction_cycles = float(config['zaber_config']['friction_cycles'])

    def calculate_run_time(self):
        if self.type_linear_movement:
            # run_time = (((self.friction_PeakToPeak_distance_mm_deg / self.friction_speed_mm_deg_sec) * self.friction_cycles) * 3) + (60 * self.friction_cycles)
            if self.friction_motion_profile == 'triangle':
                run_time = ((((self.friction_PeakToPeak_distance_mm_deg * 2) * self.friction_cycles) / self.friction_speed_mm_deg_sec) + 45) * 3
            elif self.friction_motion_profile == 'trapezoid':
                run_time = ((((self.friction_PeakToPeak_distance_mm_deg * 2) * self.friction_cycles) / self.friction_speed_mm_deg_sec) + 60 + (self.friction_cycles * 3)) * 3
        else:
            # run_time = (((self.friction_PeakToPeak_distance_mm_deg / self.friction_speed_mm_deg_sec) * self.friction_cycles) * 1) + (45 * self.friction_cycles)
            if self.friction_motion_profile == 'triangle':
                run_time = (((self.friction_PeakToPeak_distance_mm_deg * 2) * self.friction_cycles) / self.friction_speed_mm_deg_sec) + 30
            elif self.friction_motion_profile == 'trapezoid':
                run_time = (((self.friction_PeakToPeak_distance_mm_deg * 2) * self.friction_cycles) / self.friction_speed_mm_deg_sec) + 45 + (self.friction_cycles * 3)
        return run_time

    def customize_position(self, x, y, z):
        '''change position'''
        self.gantry_X_mm_steps = x
        self.gantry_Y_mm_steps = y
        self.gantry_Z_mm_steps = z


def read_points(file, linear_movement = True):
    """ read points from the file """
    config_point = configparser.ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})
    config_point.read(file)
    test_system = str(config_point['general']['test_system'])
    point_to_test = int(config_point['general']['point_to_test']) - 1
    if 'arc' in test_system:
        # arc_index =
        # arc_center =
        if linear_movement:
            x = float(config_point.getlist(test_system, 'point_x_start')[point_to_test])
            y = float(config_point.getlist(test_system, 'point_y_start')[point_to_test])
            z = float(config_point.getlist(test_system, 'point_z_start')[point_to_test])
        else:
            x = (float(config_point.getlist(test_system, 'point_x_start')[point_to_test]), float(config_point.getlist(test_system, 'point_x_end')[point_to_test]))
            y = (float(config_point.getlist(test_system, 'point_y_start')[point_to_test]), float(config_point.getlist(test_system, 'point_y_end')[point_to_test]))
            z = (float(config_point.getlist(test_system, 'point_z_start')[point_to_test]), float(config_point.getlist(test_system, 'point_z_end')[point_to_test]))

    else:
        x = float(config_point.getlist(test_system, 'point_x')[point_to_test])
        y = float(config_point.getlist(test_system, 'point_y')[point_to_test])
        z = float(config_point.getlist(test_system, 'point_z')[point_to_test])
    return test_system, point_to_test, x, y, z


def checkConfig(config):
    """
    Check if config parameters are within boundaries
    """
    if hasattr(config, 'amplitude') and config.interaction_amplitude_mm_deg > 40:
        return False
    if hasattr(config, 'frequency') and config.interaction_frequency_hz > 4:
        return False
    if hasattr(config, 'distance') and config.friction_PeakToPeak_distance_mm_deg > 40:
        return False
    if hasattr(config, 'speed') and config.friction_speed_mm_deg_sec > 25:
        return False

    return True

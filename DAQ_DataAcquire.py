import math
import pprint
import time
import collections
import numpy as np  # For Matrix Multiplication, max, min, a_range
import xlsxwriter  # For exporting to xlsx
import pandas  # For importing from xlsx
import timeit
from datetime import datetime
import argparse
import matplotlib.pyplot as plt  # For Plotting Data
import matplotlib.animation as animation

import nidaqmx
from nidaqmx.constants import EncoderType
from nidaqmx.constants import LineGrouping

class Dataset:
    def __init__(self, num_samples):
        ## initialize a dataset to store data
        self.data = []
        self.file = "DAQ_DATA/record_data.csv"
        self.count = 1
        self.Ts = 0.01

        self.num_display = 100
        self.fig = plt.figure()
        self.xs = list(range(0, self.num_display))
        self.ys = [0] * self.num_display
        self.line = None

    def __len__(self):
        return len(self.data)

    def read(self):
        #TODO:
        ## read from NIDAQ
        ## refer to readdaq()

    def write_to_file(self, time, value):
        #TODO:
        ## write tp the file (self.file)
        ## refer to writefiledata()

    def create_figure(self, num_display=100):
        ## Create figure for plotting
        ax = self.fig.add_subplot(1,1,1)
        ax.set_ylim([-10,10])
        self.line, = ax.plot(self.xs, self.ys)

    def log_data(self):
        ## log data to the file
        ## refer to logging()
        value = self.read()
        self.data.append(value)
        time.sleep(self.Ts)
        self.write_to_file(self.count * self.Ts, value)
        self.ys.append(value)
        self.ys = ys[-self.num_display:]
        self.line.set_ydata(self.ys)
        return self.line

    def start_acquire(self):
        ## 1. create a Figure
        self.create_figure()
        ## 2. run the animation, and start logging
        ani = animation.FuncAnimation(self.fig, self.log_data, fargs=(self.ys,), interval=self.num_display, blit=True)
        ## 3. show the figure
        plt.show()

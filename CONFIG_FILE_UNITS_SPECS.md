[readings_config]

; **************************************************
; LOAD CELLS SN and CORRESPONDING cDAQ MODULE:
;  - 37285 = MOD8
; **************************************************
;load_cell_sn = 35009
;load_cell_sn = 35011
load_cell_sn = 35010
;load_cell_sn = 37285

; **************************************************
; UNITS FOR DAQ:
;  - sampling_freq = [samples/sec] = [Hz]
;  - run_time = [sec]
;  - reuse_offsets [Boolean] Use the previous offsets values
; ********************************************************
;sampling_freq = 7000
sampling_freq = 2500
run_time = 10
reuse_offsets = No

; ********************************************************
; UNITS FOR FILTER:
;  - filter_cutoff_freq = [Hz]
; **************************************************
use_lowPass_filter = Yes
;filter_cutoff_freq = 10
filter_cutoff_freq = 200
running_mean_window_size = 0



[zaber_config]

notes = 2021-12-07. Testing Beta ARC, joint G0. Path #1


; **************************************************
; INPUT THE COM PORT THAT THE ZABER CONTROLLER IS CONNECTED TO USING DEVICE MANAGER
; **************************************************
usb_com_port = COM4

; **************************************************
; SELECT WHICH ACTION/TEST YOU WANT ZABER (GANTRY STAGES/ROTARY STAGE) TO EXECUTE
; **************************************************
;action_to_execute =
;action_to_execute = gantry_home
;action_to_execute = gantry_center
;action_to_execute = gantry_move_absolute
;action_to_execute = gantry_move_absolute_hid_origin
;action_to_execute = gantry_move_relative
action_to_execute = gantry_trace_arc_abs_origin
;action_to_execute = gantry_trace_arc_rel_origin
;action_to_execute = gantry_interaction_force
;action_to_execute = gantry_force_to_start
;action_to_execute = rotary_home
;action_to_execute = rotary_move_absolute
;action_to_execute = rotary_move_relative
;action_to_execute = rotary_interaction_torque
;action_to_execute = rotary_torque_to_start


; **********************************************************************************************************************
; ZABER GANTRY STAGES SPECS:
;  - Max travel range of x1 & x2 stages = 1000 mm
;  - Max travel range of y3 and z4 stages = 750 mm
;  - Max speed of x1 and x2 stages: 573 mm/s
;  - Max speed of y3 stage: 700 mm/s
;  - Max speed of z4 stage: 3000 mm/s
; x1 & x2 stages: https://www.zaber.com/products/linear-stages/LRT-EC/specs?part=LRT1000DL-E08CT3A
; y3 stage: https://www.zaber.com/products/linear-stages/LRT-EC/specs?part=LRT0750DL-E08CT3A
; z4 stage: https://www.zaber.com/products/linear-stages/LC40B-KM02/specs?part=LC40B0750-KM02
; z4 stepper motor & encoder: https://www.zaber.com/products/stepper-motors/NMS-E/specs?part=NMS23-E08P1T3A
; **********************************************************************************************************************

; **************************************************
; IS THE SIDEWAYS CLAMP BEING USED?
; **************************************************
gantry_sideways_clamp = True
;gantry_sideways_clamp = False

; **************************************************
;
; ~~~ action_to_execute = gantry_move_absolute
; ~~~ action_to_execute = gantry_move_absolute_hid_origin
;
; UNITS FOR ZABER GANTRY STAGES MOVE ABSOLUTE:
;  - Coordinates = [mm]
;  - Speed = [mm/s]
; **************************************************
gantry_X_abs = 0.0
gantry_Y_abs = 0.0
gantry_Z_abs = 0.0
gantry_speed_abs = 0.0

; **************************************************
;
; ~~~ action_to_execute = gantry_move_relative
;
; UNITS FOR ZABER GANTRY STAGES MOVE RELATIVE:
;  - Displacement = [mm]
;  - Speed = [mm/s]
; **************************************************
gantry_X_rel = 0.0
gantry_Y_rel = 0.0
gantry_Z_rel = 0.0
gantry_speed_rel = 0.0

; **************************************************
;
; ~~~ action_to_execute = gantry_trace_arc_abs_origin
;
; UNITS FOR ZABER GANTRY STAGES MOVE ARC ABSOLUTE:
;  - Displacement = [mm]
;  - Speed = [mm/s]
; **************************************************
arc_abs_target_axes_indices =
arc_abs_rotation_direction =
arc_abs_center_x =
arc_abs_center_y =
arc_abs_end_x =
arc_abs_end_y =
arc_abs_speed =

; **************************************************
;
; ~~~ action_to_execute = gantry_trace_arc_rel_origin
;
; UNITS FOR ZABER GANTRY STAGES MOVE ARC RELATIVE:
;  - Displacement = [mm]
;  - Speed = [mm/s]
; **************************************************
arc_rel_target_axes_indices =
arc_rel_rotation_direction =
arc_rel_center_x =
arc_rel_center_y =
arc_rel_end_x =
arc_rel_end_y =
arc_rel_speed =

; **************************************************
;
; ~~~ action_to_execute = gantry_interaction_force
;
; UNITS FOR ZABER GANTRY STAGES SINE MOTIONS (INTERACTION FORCE TEST):
;  - gantry_sin_amplitude = [mm]
;  - Frequency = [Hz]
; MAX AMPLITUDE OF ZABER GANTRY STAGES = 1000 mm
; MAX FREQUENCY OF ZABER STAGES = 4 Hz
; CYCLES FOR SINE MOTIONS: 1 cycle = 1 wave
; **************************************************
gantry_sin_amplitude = 0.0
gantry_sin_frequency = 0.0
gantry_sin_cycles = 0

; **************************************************
;
; ~~~ action_to_execute = gantry_force_to_start (1)
;
; MOTION PROFILE FOR ZABER GANTRY STAGES STICTION TEST (FORCE TO START MOVEMENT TEST):
; **************************************************
gantry_const_vel_motion_profile =
;gantry_const_vel_motion_profile = trapezoid
;gantry_const_vel_motion_profile = triangle
; **************************************************
;
; ~~~ action_to_execute = gantry_force_to_start (2)
;
; UNITS FOR ZABER GANTRY STAGES STICTION TEST (FORCE TO START MOVEMENT TEST):
;  - Displacement = [mm]
;  - Speed = [mm/s]
; CYCLES FOR LINEAR MOTIONS: 1 cycle = 1 forward and backward motion
; **************************************************
gantry_const_vel_relative_distance = 0.0
gantry_const_vel_speed = 0.0
gantry_const_vel_cycles = 0


; **********************************************************************************************************************
; ZABER ROTARY STAGE SPECS:
;  - Max travel range of rotary stage = 360°
;  - Max speed of rotary stage = 450°/s = 7.85 rad/s
; https://www.zaber.com/products/rotary-stages/X-RSW-E/specs?part=X-RSW60C-E03
; **********************************************************************************************************************

; **************************************************
;
; ~~~ action_to_execute = rotary_move_absolute
;
; UNITS FOR ZABER ROTARY STAGE MOVE ABSOLUTE:
;  - Angle = [°]
;  - Speed = [°/s]
; **************************************************
rotary_angle_abs = 0.0
rotary_speed_abs = 0.0

; **************************************************
;
; ~~~ action_to_execute = rotary_move_relative
;
; UNITS FOR ZABER ROTARY STAGE MOVE RELATIVE:
;  - Angle = [°]
;  - Speed = [°/s]
; **************************************************
rotary_angle_rel = 0.0
rotary_speed_rel = 0.0

; **************************************************
;
; ~~~ action_to_execute = rotary_interaction_torque
;
; UNITS FOR ZABER ROTARY STAGE SINE MOTIONS (INTERACTION TORQUE TEST):
;  - rotary_sin_amplitude = [°]
;  - Frequency = [Hz]
; MAX AMPLITUDE OF ZABER ROTARY STAGE = 360°
; MAX FREQUENCY OF ZABER STAGES = ? Hz
; CYCLES FOR SINE MOTIONS: 1 cycle = 1 wave
; **************************************************
rotary_sin_amplitude = 0.0
rotary_sin_frequency = 0.0
rotary_sin_cycles = 0

; **************************************************
;
; ~~~ action_to_execute = rotary_torque_to_start (1)
;
; MOTION PROFILE FOR ZABER ROTARY STAGE STICTION TEST (TORQUE TO START MOVEMENT TEST):
; **************************************************
rotary_const_vel_motion_profile =
;rotary_const_vel_motion_profile = trapezoid
;rotary_const_vel_motion_profile = triangle
; **************************************************
;
; ~~~ action_to_execute = rotary_torque_to_start (2)
;
; UNITS FOR ZABER ROTARY STAGE STICTION TEST (TORQUE TO START MOVEMENT TEST):
;  - Displacement = [°]
;  - Speed = [°/s]
; CYCLES FOR LINEAR MOTIONS: 1 cycle = 1 forward and backward motion
; ********************************************************
rotary_const_vel_relative_angle = 0.0
rotary_const_vel_speed = 0.0
rotary_const_vel_cycles = 0

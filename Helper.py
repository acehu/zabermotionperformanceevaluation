import os
import numpy
from datetime import datetime
from scipy.signal import butter,lfilter,filtfilt, iirnotch

import Constants


def lowpass_filter(data, cutoff, samp_freq, order=2):
    """
    The function uses scipy low-pass filter to filter out high freq noise
    """
    # Get the filter coefficients
    nyq = 0.5 * samp_freq
    normalized_cutoff = float(cutoff) / nyq
    b, a = butter(order, normalized_cutoff, btype='low', analog=False)
    y = lfilter(b, a, data, axis=0)
    return y

def notch_filter(data, notch_freq, samp_freq, quality_factor = 10.0):
    """
    The function uses scipy notch filter to filter out specific range of noise
    """
    b_notch, a_notch = iirnotch(notch_freq, quality_factor, samp_freq)
    y = filtfilt(b_notch, a_notch, data, axis=0)
    return y


def convert_counter_to_lin_displacement(counter_data):
    """

    :param counter_data:
    :return: Converted Linear Displacement from encoder
    """

    # counter_data = numpy.asarray(counter_data, dtype=numpy.float32)
    counter_data = numpy.array([numpy.array(x, dtype=numpy.float32) for x in counter_data])
    linear_displacement_real_values = numpy.zeros(shape=counter_data.shape)
    # print("\nLinear Displacement Size: \n", linear_displacement_real_values.shape, "\n")
    ratio_x_y = 1.
    ratio_z = 1.
    linear_displacement_real_values[:, 0] = numpy.multiply(counter_data[:, 0] / ratio_x_y, 25.4/360)   # x1 linear stage Linear Motion Per Motor Rev = 25.4 mm
    linear_displacement_real_values[:, 1] = numpy.multiply(counter_data[:, 1] / ratio_x_y, 25.4/360)   # x2 linear stage Linear Motion Per Motor Rev = 25.4 mm
    linear_displacement_real_values[:, 2] = numpy.multiply(counter_data[:, 2] / ratio_x_y, 25.4/360)   # y3 linear stage Linear Motion Per Motor Rev = 25.4 mm
    linear_displacement_real_values[:, 3] = numpy.multiply(counter_data[:, 3] / ratio_z, 25.4/360)     # z4 linear stage Linear Motion Per Motor Rev = 90 mm

    return linear_displacement_real_values


def convert_counter_to_rot_displacement(counter_data):
    """

    :param counter_data:
    :return: Converted Rotary Displacement from encoder
    """

    # counter_data = numpy.asarray(counter_data, dtype=numpy.float32)
    counter_data = numpy.array([numpy.array(x, dtype=numpy.float32) for x in counter_data])
    rotary_displacement_real_values = numpy.zeros(shape=counter_data.shape)
    # print("\nLinear Displacement Size: \n", rotary_displacement_real_values.shape, "\n")
    ratio_x_y = 1.
    rotary_displacement_real_values[:, 0] = numpy.multiply(counter_data[:, 0] / ratio_x_y, 12.0/360)   # rotary stage Angular Motion Per Motor Rev = 12.0°
    rotary_displacement_real_values[:, 1] = numpy.multiply(counter_data[:, 1] / ratio_x_y, 12.0/360)   # rotary stage Angular Motion Per Motor Rev = 12.0°

    return rotary_displacement_real_values


def convert_analog_to_forces_torques(analog_data, load_cell_sn):
    """
    Multiply matrices: raw analog data x calibration matrices to obtain forces and torques
    :parameter analog_data:
    :return:
    """
    # Do Matrix Multiplication: Calibration Matrix [6x6] x DAQ Reading [6x1] = Forces,Torques [6x1]
    # https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

    # print("\nForces & Torques Size: \n", analog_data.shape, "\n")

    if load_cell_sn == 28637:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_28637
    if load_cell_sn == 35009:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_35009
    if load_cell_sn == 35010:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_35010
    if load_cell_sn == 35011:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_35011
    if load_cell_sn == 37285:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_37285
    if load_cell_sn == 38398:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38398
    if load_cell_sn == 38399:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38399
    if load_cell_sn == 38394:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38394
    if load_cell_sn == 38395:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38395
    if load_cell_sn == 38396:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38396
    if load_cell_sn == 38397:
        load_cell_cal_matrix = Constants.load_cell_cal_matrix_38397

    # print("Using Load Cell SN {}".format(load_cell_sn))

    forces_torques_real_values = numpy.matmul(analog_data, numpy.transpose(load_cell_cal_matrix))

    return forces_torques_real_values


def system_check(run_function):
    '''
    A general function to help check if function runs successful, and exit program otherwise
    '''
    if not run_function:
        os._exit(0)

def make_dir_for_exporting():
    """

    :return:
    """
    date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    ''' Make DAQ_Data folder '''
    if not os.path.exists(r'DAQ_Data'):
        os.makedirs(r'DAQ_Data')

    ''' Make Zaber_Log folder '''
    if not os.path.exists(r'Zaber_Log'):
        os.makedirs(r'Zaber_Log')

    ''' Directory for DAQ_Data '''
    newpath_daq_data = r'DAQ_Data/{}'.format(date_time)
    newpath_daq_data_newest = r'DAQ_Data/Newest'
    if not os.path.exists(newpath_daq_data):
        os.makedirs(newpath_daq_data)
    if not os.path.exists(newpath_daq_data_newest):
        os.makedirs(newpath_daq_data_newest)

    ''' Directory for Zaber_Log'''
    newpath_zaber_log = r'Zaber_Log/{}_motion_library_log.txt'.format(date_time)
    newpath_zaber_log_pos = r'Zaber_Log/{}_exampleFile_pos_track.txt'.format(date_time)

    return date_time, newpath_daq_data, newpath_daq_data_newest, newpath_zaber_log, newpath_zaber_log_pos
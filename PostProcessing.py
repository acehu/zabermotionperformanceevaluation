""" Hello. This script is for acquiring data from a DAQ... """


""" (A) Import API's """
import sys
import os
import csv
import argparse
import numpy
import math
import pandas  # For importing from xlsx
import matplotlib.pyplot as plt
from scipy.ndimage.filters import uniform_filter1d
from scipy.fft import fft, fftfreq, ifft

from Helper import lowpass_filter, notch_filter, convert_analog_to_forces_torques, convert_counter_to_lin_displacement
import Constants

# rc('text', usetex=True)
# plt.rc('text', usetex=True)



def import_from_xlsx(use_analog, use_digital):
    """

    :param use_analog:
    :param use_digital:
    :return:
    """
    # Import from xlsx
    # read_excel() returns: DataFrame or dict of DataFrames (i.e. not an array, so cannot do A[:,0])
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html

    parser = argparse.ArgumentParser(description='Put parameters here')
    parser.add_argument('--datapath', type=str, default='DAQ_Data/newest.xlsx', dest='filename', help='input file path')
    args = parser.parse_args()

    analog_data, analog_data_with_offset, forcetorque_values_values, counter_data, lin_displacement_values = [], [], [], [], []

    if use_analog:
        analog_data = pandas.read_excel(args.filename, "Raw Analog Data", header=None).to_numpy()
        analog_data_with_offset = pandas.read_excel(args.filename, "Analog Data after Offset", header=None).to_numpy()
        forcetorque_values_values = pandas.read_excel(args.filename, "Converted to Forces and Torques", header=None).to_numpy()
        print("Analog Data Size: ", analog_data.size)
        print("Analog Data number of Rows: ", len(analog_data))
        print("Analog Data number of Cols: ", len(analog_data[0]))

    if use_digital:
        counter_data = pandas.read_excel(args.filename, "Raw Encoder Data", header=None).to_numpy()
        lin_displacement_values = pandas.read_excel(args.filename, "Converted to Lin Displacement", header=None).to_numpy()
        print("Counter Data Size: ", counter_data.size)
        print("Counter Data number of Rows: ", len(counter_data))
        print("Counter Data number of Cols: ", len(counter_data[0]))

    return analog_data, analog_data_with_offset, forcetorque_values_values, counter_data, lin_displacement_values


def get_folder_name_from_user():

    parser = argparse.ArgumentParser(description='Run post-processing script (import csv data, convert data, filter data, plot data). Syntax: \n'
                                                 'python .\\PostProcessing.py --datapath <FOLDER_NAME>')
    parser.add_argument('--datapath', type=str, default='Newest', dest='filename', help='Input folder right now!')
    args = parser.parse_args()
    folder = args.filename

    # folder = input("Input folder right now!: ")

    print("\nAccessing csv files in folder: {}".format(folder))
    print("Data Type of folder: {} \n".format(type(folder)))

    return folder


def get_curr_dir_for_importing():

    # files = os.listdir(cwd)               # Get all the files in that directory
    # print("Files in {}: {}".format(cwd, files))
    curr_dir = os.getcwd()                  # Get the current working directory (cwd)
    curr_dir = curr_dir.replace("\\", '/')
    return curr_dir


def import_from_csv_config_parameters(folder, curr_dir, import_config):

    if import_config:

        if os.path.exists(r'DAQ_Data/{}/{}_Config_Parameters.csv'.format(folder, folder)):

            with open(curr_dir + "/DAQ_Data/" + folder + "/" + folder + "_Config_Parameters.csv", "r", encoding='utf-8') as csvfile_config_parameters:
                print("Loading csv file: {}_Config_Parameters.csv".format(folder))
                config_parameters = csv.reader(csvfile_config_parameters, delimiter=",")
                config_parameters = list(config_parameters)
                config_parameters = numpy.array(config_parameters)
                print("Closing csv file: {}_Config_Parameters.csv \n".format(folder))

            print("Config Parameters NumpyArray: \n{}\n".format(config_parameters))
            print("Config Parameter NumpyArray @ [0][1]: \n {} \n". format(config_parameters[0][1]))


            config_parameters_df = pandas.DataFrame(data=config_parameters, columns=["PARAMETER", "VALUE"])

            print("Config Parameters DataFrame: \n {}\n".format(config_parameters_df))
            print("Config Parameters DataFrame Number of Rows: \n {} \n".format(config_parameters_df.shape[0]))
            print("Config Parameter DataFrame @ [1][1]: \n {} \n".format(config_parameters_df.iloc[1][1]))
            print("Config Parameter DataFrame @ [0]: \n{}\n".format(config_parameters_df.iloc[0]))
            print("Data Type of data @ [1][1]: {} \n".format(type(config_parameters_df.iloc[1][1])))

            for row in range(config_parameters_df.shape[0]):

                if config_parameters_df.iloc[row][0] == 'Load Cell SN':
                    load_cell_sn = int(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ cDAQ9189 ai9252 module' or config_parameters_df.iloc[row][0] == 'DAQ cDAQ9185 ai9252 module':
                    cDAQ9189_ai9252_module = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ sampling freq [samples/sec]':
                    sampling_freq = float(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ data collection run time [sec]':
                    run_time = float(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ number of samples (sampling freq * run time)':
                    number_of_samples = float(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ reuse offsets from previous run?':
                    reuse_offsets = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ low pass filter used?':
                    use_lowPass_filter = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ filter cutoff freq [Hz]':
                    filter_cutoff_freq = float(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'DAQ running mean window size':
                    running_mean_window_size = int(config_parameters_df.iloc[row][1])

                if config_parameters_df.iloc[row][0] == 'Notes':
                    notes = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'Zaber USB COM port':
                    usb_com_port = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'Zaber commanded motion':
                    action_to_execute = str(config_parameters_df.iloc[row][1])
                if config_parameters_df.iloc[row][0] == 'Zaber gantry sideways clamp being used?':
                    gantry_sideways_clamp = str(config_parameters_df.iloc[row][1])

                if config_parameters_df.iloc[row][0] == 'Zaber gantry X move absolute or relative [mm]':
                    gantry_X_mm = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry Y move absolute or relative [mm]':
                    gantry_Y_mm = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry Z move absolute or relative [mm]':
                    gantry_Z_mm = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry speed absolute or relative [mm/s]':
                    gantry_speed_mm_s = config_parameters_df.iloc[row][1]

                if config_parameters_df.iloc[row][0] == 'Zaber rotary angle move absolute or relative [°]':
                    rotary_angle_deg = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary speed absolute or relative [°/s]':
                    rotary_speed_deg_s = config_parameters_df.iloc[row][1]

                if config_parameters_df.iloc[row][0] == 'Zaber gantry arc number of cycles':
                    cycles_arc = config_parameters_df.iloc[row][1]

                if config_parameters_df.iloc[row][0] == 'Zaber gantry sine amplitude [mm]':
                    gantry_sin_amplitude = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry sine frequency [Hz]':
                    gantry_sin_frequency = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry sine number of cycles':
                    gantry_sin_cycles = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry constant velocity motion profile':
                    gantry_const_vel_motion_profile = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry constant velocity relative distance [mm]':
                    gantry_const_vel_relative_distance = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry constant velocity speed [mm/s]':
                    gantry_const_vel_speed = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber gantry constant velocity cycles':
                    gantry_const_vel_cycles = config_parameters_df.iloc[row][1]

                if config_parameters_df.iloc[row][0] == 'Zaber rotary sine amplitude [°]':
                    rotary_sin_amplitude = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary sine frequency [Hz]':
                    rotary_sin_frequency = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary sine number of cycles':
                    rotary_sin_cycles = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary constant velocity motion profile':
                    rotary_const_vel_motion_profile = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary constant velocity relative distance [°]':
                    rotary_const_vel_relative_angle = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary constant velocity speed [°/s]':
                    rotary_const_vel_speed = config_parameters_df.iloc[row][1]
                if config_parameters_df.iloc[row][0] == 'Zaber rotary constant velocity cycles':
                    rotary_const_vel_cycles = config_parameters_df.iloc[row][1]

        return load_cell_sn, cDAQ9189_ai9252_module, sampling_freq, run_time, number_of_samples, reuse_offsets, use_lowPass_filter, filter_cutoff_freq, running_mean_window_size,\
               notes, usb_com_port, action_to_execute, gantry_sideways_clamp, \
               gantry_sin_amplitude, gantry_sin_frequency, gantry_sin_cycles, gantry_const_vel_motion_profile, gantry_const_vel_relative_distance, gantry_const_vel_speed, gantry_const_vel_cycles,\
               rotary_sin_amplitude, rotary_sin_frequency, rotary_sin_cycles, rotary_const_vel_motion_profile, rotary_const_vel_relative_angle, rotary_const_vel_speed, rotary_const_vel_cycles

        # return config_parameters_df


def import_from_csv_data(folder, curr_dir, import_analog, import_counter):
    """
    :param import_analog:
    :param import_counter:
    :return:
    """

    analog_data_raw_csv, analog_data_filtered_csv, analog_data_filtered_with_offset_csv, forcetorque_values_values_csv, counter_data_raw_csv, lin_displacement_values_csv, rot_displacement_values_csv, angular_displacement_values_csv, time_stamps = [], [], [], [], [], [], [], [], []

    if import_analog:

        if os.path.exists(r'DAQ_Data/{}/{}_Raw_Analog_Data.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Raw_Analog_Data.csv", "r", encoding='utf-8') as csvfile_analog_raw:
                print("Loading csv file: {}_Raw_Analog_Data.csv".format(folder))
                analog_data_raw_csv = csv.reader(csvfile_analog_raw, delimiter=",")
                analog_data_raw_csv = list(analog_data_raw_csv)
                analog_data_raw_csv = numpy.array(analog_data_raw_csv).astype("float")
                analog_data_raw_csv = numpy.transpose(analog_data_raw_csv)
                print("Closing csv file: {}_Raw_Analog_Data.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Analog_Data_Filtered.csv'.format(folder, folder)):
            with open(curr_dir + "/DAQ_Data/" + folder + "/" + folder + "_Analog_Data_Filtered.csv", "r", encoding='utf-8') as csvfile_analog_filtered:
                print("Loading csv file: {}_Analog_Data_Filtered.csv".format(folder))
                analog_data_filtered_csv = csv.reader(csvfile_analog_filtered, delimiter=",")
                analog_data_filtered_csv = list(analog_data_filtered_csv)
                analog_data_filtered_csv = numpy.array(analog_data_filtered_csv).astype("float")
                analog_data_filtered_csv = numpy.transpose(analog_data_filtered_csv)
                print("Closing csv file: {}_Analog_Data_Filtered.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Analog_Data_Filtered_With_Offset.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Analog_Data_Filtered_With_Offset.csv", "r", encoding='utf-8') as csvfile_analog_filtered_with_offset:
                print("Loading csv file: {}_Analog_Data_Filtered_With_Offset.csv".format(folder))
                analog_data_filtered_with_offset_csv = csv.reader(csvfile_analog_filtered_with_offset, delimiter=",")
                analog_data_filtered_with_offset_csv = list(analog_data_filtered_with_offset_csv)
                analog_data_filtered_with_offset_csv = numpy.array(analog_data_filtered_with_offset_csv).astype("float")
                analog_data_filtered_with_offset_csv = numpy.transpose(analog_data_filtered_with_offset_csv)
                print("Closing csv file: {}_Analog_Data_Filtered_With_Offset.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Converted_To_Forces_Torques.csv'.format(folder, folder)):
            with open(curr_dir + "/DAQ_Data/" + folder + "/" + folder + "_Converted_To_Forces_Torques.csv", "r", encoding='utf-8') as csvfile_forcestorques:
                print("Loading csv file: {}_Converted_To_Forces_Torques.csv".format(folder))
                forcetorque_values_values_csv = csv.reader(csvfile_forcestorques, delimiter=",")
                forcetorque_values_values_csv = list(forcetorque_values_values_csv)
                forcetorque_values_values_csv = numpy.array(forcetorque_values_values_csv).astype("float")
                forcetorque_values_values_csv = numpy.transpose(forcetorque_values_values_csv)
                print("Closing csv file: {}_Converted_To_Forces_Torques.csv \n".format(folder))

        'The below method takes too long to load the csv files'
        # analog_data_raw_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Raw_Analog_Data.csv", header=None)
        # analog_data_filtered_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Analog_Data_Filtered.csv", header=None)
        # analog_data_filtered_with_offset_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Analog_Data_Filtered_With_Offset.csv", header=None)
        # forcetorque_values_values_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Converted_To_Forces_Torques.csv", header=None)

    if import_counter:

        if os.path.exists(r'DAQ_Data/{}/{}_Raw_Encoder_Data.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Raw_Encoder_Data.csv", "r", encoding='utf-8') as csvfile_counter_raw:
                print("Loading csv file: {}_Raw_Encoder_Data.csv".format(folder))
                counter_data_raw_csv = csv.reader(csvfile_counter_raw, delimiter=",")
                counter_data_raw_csv = list(counter_data_raw_csv)
                counter_data_raw_csv = numpy.array(counter_data_raw_csv).astype("float")
                counter_data_raw_csv = numpy.transpose(counter_data_raw_csv)
                print("Closing csv file: {}_Raw_Encoder_Data.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Converted_To_Linear_Displacement.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Converted_To_Linear_Displacement.csv", "r", encoding='utf-8') as csvfile_lin_displacement:
                print("Loading csv file: {}_Converted_To_Linear_Displacement.csv".format(folder))
                lin_displacement_values_csv = csv.reader(csvfile_lin_displacement, delimiter=",")
                lin_displacement_values_csv = list(lin_displacement_values_csv)
                lin_displacement_values_csv = numpy.array(lin_displacement_values_csv).astype("float")
                lin_displacement_values_csv = numpy.transpose(lin_displacement_values_csv)
                print("Closing csv file: {}_Converted_To_Linear_Displacement.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Converted_To_Rotary_Displacement.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Converted_To_Rotary_Displacement.csv", "r", encoding='utf-8') as csvfile_rot_displacement:
                print("Loading csv file: {}_Converted_To_Linear_Displacement.csv".format(folder))
                rot_displacement_values_csv = csv.reader(csvfile_rot_displacement, delimiter=",")
                rot_displacement_values_csv = list(rot_displacement_values_csv)
                rot_displacement_values_csv = numpy.array(rot_displacement_values_csv).astype("float")
                rot_displacement_values_csv = numpy.transpose(rot_displacement_values_csv)
                print("Closing csv file: {}_Converted_To_Linear_Displacement.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Angular_Displacement.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Angular_Displacement.csv", "r", encoding='utf-8') as csvfile_angular_displacement:
                print("Loading csv file: {}_Angular_Displacement.csv".format(folder))
                angular_displacement_values_csv = csv.reader(csvfile_angular_displacement, delimiter=",")
                angular_displacement_values_csv = list(angular_displacement_values_csv)
                angular_displacement_values_csv = numpy.array(angular_displacement_values_csv).astype("float")
                angular_displacement_values_csv = numpy.transpose(angular_displacement_values_csv)
                print("Closing csv file: {}_Angular_Displacement.csv \n".format(folder))

        if os.path.exists(r'DAQ_Data/{}/{}_Time_Stamps.csv'.format(folder, folder)):
            with open(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Time_Stamps.csv", "r", encoding='utf-8') as time_stamps:
                print("Loading csv file: {}_Time_Stamps.csv".format(folder))
                time_stamps = csv.reader(time_stamps, delimiter=",")
                time_stamps = list(time_stamps)
                time_stamps = numpy.array(time_stamps).astype("float")
                time_stamps = numpy.transpose(time_stamps)
                print("Closing csv file: {}_Time_Stamps.csv \n".format(folder))

        'The below method takes too long to load the csv files'
        # counter_data_raw_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Raw_Encoder_Data.csv", header=None)
        # lin_displacement_values_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Converted_To_Linear_Displacement.csv", header=None).to_numpy()
        # counter_data_raw_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Raw_Encoder_Data.csv", header=None)
        # lin_displacement_values_csv = pandas.read_csv(curr_dir+"/DAQ_Data/"+folder+"/"+folder+"_Angular_Displacement.csv", header=None).to_numpy()

    return analog_data_raw_csv, analog_data_filtered_csv, analog_data_filtered_with_offset_csv, forcetorque_values_values_csv, counter_data_raw_csv, lin_displacement_values_csv, rot_displacement_values_csv, angular_displacement_values_csv, time_stamps


def calc_fft(data, sampling_freq):
    # Number of samples
    N = data.shape[0]
    # Period [sec]
    T = 1.0/sampling_freq
    x = numpy.linspace(0.0, N*T, N, endpoint=False)
    xf = fftfreq(N, T)[:N//2]
    yf_0 = fft(data[0])
    yf_1 = fft(data[1])
    yf_2 = fft(data[2])
    yf_3 = fft(data[3])
    yf_4 = fft(data[4])
    yf_5 = fft(data[5])
    # yf_0 = fft(data.iloc[:, 0])
    # yf_1 = fft(data.iloc[:, 1])
    # yf_2 = fft(data.iloc[:, 2])
    # yf_3 = fft(data.iloc[:, 3])
    # yf_4 = fft(data.iloc[:, 4])
    # yf_5 = fft(data.iloc[:, 5])

    return xf, yf_0, yf_1, yf_2, yf_3, yf_4, yf_5


def calc_velocity(lin_displacement_values):
    lin_velocity_values = scipy


def print_average_force_torque(forcetorque_values):
    # force_torque_values_avg = numpy.average(forcetorque_values)
    # force_torque_values_avg = numpy.ndarray.mean(forcetorque_values, axis=1)

    force_torque_values_avg = numpy.mean(forcetorque_values, axis=1)  # axis = 0 take average of each column, axis = 1 take average of each row
    force_torque_values_avg = numpy.array(force_torque_values_avg)

    print("Forces and Torques averages: ", force_torque_values_avg)
    print("Fx average = ", force_torque_values_avg[2], " N")
    print("Fy average = ", force_torque_values_avg[0], " N")
    print("Fz average = ", force_torque_values_avg[1], " N")
    print("Tx average = ", force_torque_values_avg[5], " N*m")
    print("Ty average = ", force_torque_values_avg[3], " N*m")
    print("Tz average = ", force_torque_values_avg[4], " N*m")

    return force_torque_values_avg


def plot_readings(folder,
                  sampling_freq, run_time, number_of_samples, filter_cutoff_freq, running_mean_window_size, action_to_execute, gantry_sin_amplitude, gantry_sin_frequency, gantry_sin_cycles, gantry_const_vel_relative_distance, gantry_const_vel_speed, gantry_const_vel_cycles,
                  rotary_sin_amplitude, rotary_sin_frequency, rotary_sin_cycles, rotary_const_vel_relative_angle, rotary_const_vel_speed, rotary_const_vel_cycles,
                  analog_data, analog_data_filtered, analog_data_with_offset, forcetorque_values, force_torque_values_avg, counter_data, lin_displacement_values, rot_displacement_values, angular_displacement_values, time_stamps,
                  xf, yf_0, yf_1, yf_2, yf_3, yf_4, yf_5,
                  plot_analog, plot_forces_all, plot_forces_components, plot_torques_all, plot_torques_components, plot_avg_forces_torques, plot_encoder, plot_displacement_linear, plot_displacement_rotary, plot_displacement_arc, plot_ForcesDisplacement_vs_Time, plot_TorquesDisplacement_vs_Time, plot_Forces_vs_Displacement, plot_Torques_vs_Displacement, plot_Torques_vs_Displacement_Haptics, plot_fft_all, plot_fft_components, plot_fft_log_all, plot_fft_log_components):
    """
    Plot raw analog data, analog data converted to forces & torques, and raw counter data
    :parameter analog_data: raw analog data
    :parameter analog_data_with_offset:
    :parameter forcetorque_values: analog data converted to forces & torques
    :parameter counter_data: raw counter data
    :parameter lin_displacement_values:
    :return: N/A
    """

    analog_data_transpose = numpy.transpose(analog_data)
    analog_data_with_offset_transpose = numpy.transpose(analog_data_with_offset)
    force_torque_values_transpose = numpy.transpose(forcetorque_values)
    counter_data_transpose = numpy.transpose(counter_data)
    lin_displacement_transpose = numpy.transpose(lin_displacement_values)

    # Get Number of Columns and Number of Rows in data sets
    analog_rows = len(analog_data)
    digitl_rows = len(counter_data)
    cols = len(analog_data[0])
    print("\n# of Rows in analog data set = {}".format(analog_rows))
    print("# of Columns in all data sets = {}".format(cols))
    print("# of Rows in counter data set = {}".format(digitl_rows))
    print("# of Columns in all data sets = {}\n".format(cols))

    # Set up X-axis (convert samples to time)
    t_axis = numpy.linspace(0, run_time, cols)

    # Running Mean. Window size = size
    forcetorque_values = uniform_filter1d(input=forcetorque_values, size=50)

    '''
    Figure 1: Analog Data RAW
    Figure 2: Analog Data converted to FORCES & TORQUES (pre-offset)
    Figure 3: Analog Data converted to FORCES & TORQUES (OFFSETTED, pre-dlpf)
    Figure 4: Analog Data converted to FORCES & TORQUES (offsetted, AFTER-DLPF, pre-running-avg)
    Figure 5: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, AFTER-RUNNING-AVG)
    Figure 5a: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Fx
    Figure 5b: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Fy
    Figure 5c: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Fz
    Figure 5: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, AFTER-RUNNING-AVG)
    Figure 5d: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Tx
    Figure 5e: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Ty
    Figure 5f: Analog Data converted to FORCES & TORQUES (offsetted, after-dlpf, after-running-avg), Tz
    Figure 6: Avg
    Figure 7: Enc RAw
    Figure 8: Enc converted to LIN DISPL
    ...
    '''

    if action_to_execute == 'gantry_force_to_start':
        fig_suptitle = 'Test Executed: Force to Start Movement\nInputted Zaber Motor Parameters: linear travel distance = {} mm, linear speed = {} mm/s, cycles = {}'.format(gantry_const_vel_relative_distance, gantry_const_vel_speed, gantry_const_vel_cycles)
    elif action_to_execute == 'gantry_interaction_force':
        fig_suptitle = 'Test Executed: Interaction Forces\nInputted Zaber Motor Parameters: sine amplitude = {} mm, sine frequency = {} Hz, cycles = {}'.format(gantry_sin_amplitude, gantry_sin_frequency, gantry_sin_cycles)
    elif action_to_execute == 'rotary_torque_to_start':
        fig_suptitle = 'Test Executed: Torque to Start Movement\nInputted Zaber Motor Parameters: angular travel distance = {}°, angular speed = {}°/s, cycles = {}'.format(rotary_const_vel_relative_angle, rotary_const_vel_speed, rotary_const_vel_cycles)
    elif action_to_execute == 'rotary_interaction_torque':
        fig_suptitle = 'Test Executed: Interaction Torques\nInputted Zaber Motor Parameters: sine amplitude = {} mm, sine frequency = {} Hz, cycles = {}'.format(rotary_sin_amplitude, rotary_sin_frequency, rotary_sin_cycles)
    else:
        fig_suptitle = 'Please refer to {}_Config_Parameters.csv for Parameters'.format(folder)

    '''
    Plot the Raw Analog data
    '''
    if plot_analog:

        if len(analog_data):
            fig = plt.figure('{} - Figure 1: Analog Data RAW'.format(folder))
            fig.suptitle(fig_suptitle, fontsize=20)
            plt.xlabel('Time [sec]', fontsize=20)
            plt.ylabel('Voltage [V]', fontsize=20)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.plot(t_axis, analog_data[0])
            plt.plot(t_axis, analog_data[1])
            plt.plot(t_axis, analog_data[2])
            plt.plot(t_axis, analog_data[3])
            plt.plot(t_axis, analog_data[4])
            plt.plot(t_axis, analog_data[5])
            # plt.plot(t_axis, analog_data.iloc[:, 0])
            # plt.plot(t_axis, analog_data.iloc[:, 1])
            # plt.plot(t_axis, analog_data.iloc[:, 2])
            # plt.plot(t_axis, analog_data.iloc[:, 3])
            # plt.plot(t_axis, analog_data.iloc[:, 4])
            # plt.plot(t_axis, analog_data.iloc[:, 5])
            plt.legend(['ai0', 'ai1', 'ai2', 'ai3', 'ai4', 'ai5'], fontsize=20)
            plt.grid()

        if len(analog_data_filtered):
            fig = plt.figure('{} - Figure 2: Analog Data FILTERED'.format(folder))
            fig.suptitle(fig_suptitle, fontsize=20)
            plt.xlabel('Time [sec]', fontsize=20)
            plt.ylabel('Voltage [V]', fontsize=20)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.plot(t_axis, analog_data_filtered[0])
            plt.plot(t_axis, analog_data_filtered[1])
            plt.plot(t_axis, analog_data_filtered[2])
            plt.plot(t_axis, analog_data_filtered[3])
            plt.plot(t_axis, analog_data_filtered[4])
            plt.plot(t_axis, analog_data_filtered[5])
            plt.legend(['ai0', 'ai1', 'ai2', 'ai3', 'ai4', 'ai5'], fontsize=20)
            plt.grid()

        if len(analog_data_with_offset):
            fig = plt.figure('{} - Figure 3: Analog Data FILTERED and after OFFSET'.format(folder))
            fig.suptitle(fig_suptitle, fontsize=20)
            plt.xlabel('Time [sec]', fontsize=20)
            plt.ylabel('Voltage [V]', fontsize=20)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.plot(t_axis, analog_data_with_offset[0])
            plt.plot(t_axis, analog_data_with_offset[1])
            plt.plot(t_axis, analog_data_with_offset[2])
            plt.plot(t_axis, analog_data_with_offset[3])
            plt.plot(t_axis, analog_data_with_offset[4])
            plt.plot(t_axis, analog_data_with_offset[5])
            plt.legend(['ai0', 'ai1', 'ai2', 'ai3', 'ai4', 'ai5'], fontsize=20)
            plt.grid()

    '''
    Plot the Forces (converted from raw analog data)
    '''
    if plot_forces_all and len(forcetorque_values):
        fig = plt.figure('{} - Figure 4 : Analog Data converted to FORCES'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[2], color='blue')
        plt.plot(t_axis, forcetorque_values[0], color='orange')
        plt.plot(t_axis, forcetorque_values[1], color='green')
        plt.legend(['Fx', 'Fy', 'Fz'], fontsize=20)
        plt.grid()

    if plot_forces_components and len(forcetorque_values):

        fig = plt.figure('{} - Figure 4a: Analog Data converted to FORCES, Fx'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[2], color='blue')
        plt.legend(['Fx'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 4b: Analog Data converted to FORCES, Fy'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[0], color='orange')
        plt.legend(['Fy'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 4c: Analog Data converted to FORCES, Fz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[1], color='green')
        plt.legend(['Fz'], fontsize=20)
        plt.grid()

    '''
    Plot the Torques (converted from raw analog data)
    '''
    if plot_torques_all and len(forcetorque_values):
        fig = plt.figure('{} - Figure 5 : Analog Data converted to TORQUES'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Torque [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[3], color='blue')
        plt.plot(t_axis, forcetorque_values[4], color='orange')
        plt.plot(t_axis, forcetorque_values[5], color='green')
        plt.legend(['Tx', 'Ty', 'Tz'], fontsize=20)
        plt.grid()

    if plot_torques_components and len(forcetorque_values):

        fig = plt.figure('{} - Figure 5a: Analog Data converted to TORQUES, Tx'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Torque [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[3], color='blue')
        plt.legend(['Tx'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 5b: Analog Data converted to TORQUES, Ty'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Torque [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[4], color='orange')
        plt.legend(['Ty'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 5c: Analog Data converted to TORQUES, Tz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Torque [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[5], color='green')
        y = 8.53*numpy.sin(12.566*t_axis)
        # plt.plot(t_axis, y)
        plt.legend(['Tz'], fontsize=20)
        plt.grid()

    '''
    Plot the average forces and torques over entire run time
    '''
    if plot_avg_forces_torques and len(force_torque_values_avg):
        fig = plt.figure('{} - Figure 6: Average Forces and Torques over entire {} sec run time'.format(folder, run_time))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Force [N] and Torque [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.hlines(y=force_torque_values_avg[2], xmin=0, xmax=run_time, color='blue')
        plt.hlines(y=force_torque_values_avg[0], xmin=0, xmax=run_time, color='orange')
        plt.hlines(y=force_torque_values_avg[1], xmin=0, xmax=run_time, color='green')
        plt.hlines(y=force_torque_values_avg[5], xmin=0, xmax=run_time, color='red')
        plt.hlines(y=force_torque_values_avg[3], xmin=0, xmax=run_time, color='purple')
        plt.hlines(y=force_torque_values_avg[4], xmin=0, xmax=run_time, color='brown')
        plt.legend(['Fx average', 'Fy average', 'Fz average', 'Tx average', 'Ty average', 'Tz average'], fontsize=20)
        plt.grid()

    '''
    Plot the Raw Encoder data
    '''
    if plot_encoder and len(counter_data):
        if len(counter_data):
            fig = plt.figure('{} - Figure 7: Linear Stages Encoder Data RAW'.format(folder))
            fig.suptitle(fig_suptitle, fontsize=20)
            plt.xlabel('Time [sec]', fontsize=20)
            plt.ylabel('Raw Encoder Counts [°]', fontsize=20)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.plot(t_axis, counter_data[2])
            plt.plot(t_axis, counter_data[0])
            plt.plot(t_axis, counter_data[1])
            plt.plot(t_axis, counter_data[3])
            plt.legend(['x1 Encoder', 'y2 Encoder', 'y3 Encoder', 'z4 Encoder'], fontsize=20)
            plt.grid()

    '''
    Plot the Linear Displacement (converted from raw encoder data)
    '''
    if plot_displacement_linear and len(lin_displacement_values):
        fig = plt.figure('{} - Figure 8: Linear Stages Encoder Data converted to LINEAR DISPLACEMENT'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Linear Displacement [mm]', fontsize=10)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, lin_displacement_values[2], color='red')
        plt.plot(t_axis, lin_displacement_values[0], color='purple')
        plt.plot(t_axis, lin_displacement_values[1], color='brown')
        plt.plot(t_axis, lin_displacement_values[3], color='pink')
        plt.legend(['x1 Displacement', 'y2 Displacement', 'y3 Displacement', 'z4 Displacement'], fontsize=15)
        plt.grid()

    '''
    Plot the Rotary Displacement (already acquired in displacement form)
    '''
    if plot_displacement_rotary and len(rot_displacement_values):
        fig = plt.figure('{} - Figure 9: Rotary Stages Encoder Data converted to ROTARY DISPLACEMENT'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Rotary Displacement [°]', fontsize=10)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, rot_displacement_values[0], color='magenta')
        plt.legend(['Rotary Displacement'], fontsize=15)
        plt.grid()

    '''
    Plot the Arc Displacement (converted from raw encoder data; axis vs. axis)
    '''
    if plot_displacement_arc and len(lin_displacement_values):

        fig = plt.figure('{} - Figure 10: yy Displacement vs. x Displacement'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('x [mm]', fontsize=20)
        plt.ylabel('yy [mm]', fontsize=20)
        plt.axis('equal')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(lin_displacement_values[2], lin_displacement_values[0], color='red')
        plt.plot(lin_displacement_values[2], lin_displacement_values[1], color='green')
        plt.legend(['y0 vs x', 'y1 vs x'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 11: z Displacement vs. x Displacement'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('x [mm]', fontsize=20)
        plt.ylabel('z [mm]', fontsize=20)
        plt.axis('equal')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(lin_displacement_values[2], lin_displacement_values[3], color='red')
        plt.legend(['z vs x'], fontsize=20)
        plt.grid()



    """ Implement 0.3 sec delay (multiply by sampling_freq to convert to samples) to align linear displacement and force plots. Force plot lags by 0.3 - 0.4 sec """
    print("lin_displacement_values type: ", type(lin_displacement_values))
    print("lin_displacement_values number of rows: ", len(lin_displacement_values))
    print("lin_displacement_values number of cols: ", len(lin_displacement_values[0]))

    num_samples_to_delay = int(0.3476 * sampling_freq)  # Force plot lags by 0.3 - 0.4 sec
    zeroes_array_delay = numpy.zeros(shape=(1, num_samples_to_delay))
    print("delay number of rows: ", len(zeroes_array_delay))
    print("delay number of cols: ", len(zeroes_array_delay[0]))

    zeroes_array_delay = numpy.transpose(zeroes_array_delay)
    lin_displacement_values_with_delay = numpy.insert(arr=lin_displacement_values, obj=0, values=zeroes_array_delay, axis=1)
    print("lin_displacement_values_with_delay number of rows after insert:", len(lin_displacement_values_with_delay))
    print("lin_displacement_values_with_delay number of cols after insert:", len(lin_displacement_values_with_delay[0]))
    lin_displacement_values_with_delay = numpy.delete(arr=lin_displacement_values_with_delay, obj=numpy.s_[(len(lin_displacement_values[0])-1):(len(lin_displacement_values_with_delay[0])-1)], axis=1)
    print("lin_displacement_values_with_delay number of rows after clip:", len(lin_displacement_values_with_delay))
    print("lin_displacement_values_with_delay number of cols after clip:", len(lin_displacement_values_with_delay[0]))

    """ Implement 0.3 sec delay (multiply by sampling_freq to convert to samples) to align rotary displacement and torque  plots. Torque plot lags by 0.3 - 0.4 sec """
    if 'rotary' in action_to_execute:
        print("rot_displacement_values type: ", type(rot_displacement_values))
        print("rot_displacement_values number of rows: ", len(rot_displacement_values))
        print("rot_displacement_values number of cols: ", len(rot_displacement_values[0]))

        num_samples_to_delay = int(0.3476 * sampling_freq)  # Force plot lags by 0.3 - 0.4 sec
        zeroes_array_delay = numpy.zeros(shape=(1, num_samples_to_delay))
        print("delay number of rows: ", len(zeroes_array_delay))
        print("delay number of cols: ", len(zeroes_array_delay[0]))

        zeroes_array_delay = numpy.transpose(zeroes_array_delay)
        rot_displacement_values_with_delay = numpy.insert(arr=rot_displacement_values, obj=0, values=zeroes_array_delay, axis=1)
        print("rot_displacement_values_with_delay number of rows after insert:", len(rot_displacement_values_with_delay))
        print("rot_displacement_values_with_delay number of cols after insert:", len(rot_displacement_values_with_delay[0]))
        rot_displacement_values_with_delay = numpy.delete(arr=rot_displacement_values_with_delay, obj=numpy.s_[(len(rot_displacement_values[0])-1):(len(rot_displacement_values_with_delay[0])-1)], axis=1)
        print("rot_displacement_values_with_delay number of rows after clip:", len(rot_displacement_values_with_delay))
        print("rot_displacement_values_with_delay number of cols after clip:", len(rot_displacement_values_with_delay[0]))



    '''
    Plot the Forces & Torques and Linear Displacement on the same plot
    '''
    if plot_ForcesDisplacement_vs_Time and len(forcetorque_values) and len(lin_displacement_values):

        fig = plt.figure('{} - Figure 12: FORCES & LINEAR DISPLACEMENT vs. Time'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Forces [N] && \nLinear Displacement [deka-meter, 10^1]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(t_axis, forcetorque_values[2], color='blue')
        plt.plot(t_axis, forcetorque_values[0], color='orange')
        plt.plot(t_axis, forcetorque_values[1], color='green')
        # plt.plot(t_axis, forcetorque_values[3], color='blue')
        # plt.plot(t_axis, forcetorque_values[4], color='orange')
        # plt.plot(t_axis, forcetorque_values[5], color='green')
        plt.plot(t_axis, lin_displacement_values_with_delay[2]/100, color='red')
        plt.plot(t_axis, lin_displacement_values_with_delay[0]/100, color='purple')
        plt.plot(t_axis, lin_displacement_values_with_delay[1]/100, color='brown')
        plt.plot(t_axis, lin_displacement_values_with_delay[3]/100, color='pink')
        # plt.plot(t_axis, lin_displacement_values[2], color='red')
        # plt.plot(t_axis, lin_displacement_values[0], color='purple')
        # plt.plot(t_axis, lin_displacement_values[1], color='brown')
        # plt.plot(t_axis, lin_displacement_values[3], color='pink')
        plt.legend(['Fx', 'Fy', 'Fz', 'x1 Displacement', 'y2 Displacement', 'y3 Displacement', 'z4 Displacement'], fontsize=12)
        plt.grid()

    if plot_TorquesDisplacement_vs_Time and len(forcetorque_values) and len(rot_displacement_values):

        fig = plt.figure('{} - Figure 13: TORQUES & ROTARY DISPLACEMENT vs. Time'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Torques [N-mm] && \nRotary Displacement [deka-degree, 10^1]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        # plt.plot(t_axis, forcetorque_values[2], color='blue')
        # plt.plot(t_axis, forcetorque_values[0], color='orange')
        # plt.plot(t_axis, forcetorque_values[1], color='green')
        # plt.plot(t_axis, forcetorque_values[3], color='blue')
        # plt.plot(t_axis, forcetorque_values[4], color='orange')
        plt.plot(t_axis, forcetorque_values[5], color='green')
        plt.plot(t_axis, rot_displacement_values_with_delay[0]/180, color='magenta')
        plt.legend(['Tz', 'Rotary Displacement'], fontsize=12)
        # plt.legend(['Tx', 'Ty', 'Tz', 'Rotary Displacement'], fontsize=12)
        plt.grid()

    if plot_Forces_vs_Displacement and len(forcetorque_values) and len(lin_displacement_values):
        fig = plt.figure('{} - Figure 14a: FORCE vs. LINEAR DISPLACEMENT: Fx vs. x1'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Linear Displacement [mm]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(lin_displacement_values_with_delay[2], forcetorque_values[2], color='blue')
        plt.legend(['Fx vs. x1 Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 14b: FORCE vs. LINEAR DISPLACEMENT: Fy vs. y2'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Linear Displacement [mm]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(lin_displacement_values_with_delay[0], forcetorque_values[0], color='orange')
        plt.legend(['Fy vs. y2 Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 14c: FORCE vs. LINEAR DISPLACEMENT: Fz vs. z4'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Linear Displacement [mm]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(lin_displacement_values_with_delay[3], forcetorque_values[1], color='green')
        plt.legend(['Fz vs. z4 Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 14d: FORCE vs. LINEAR DISPLACEMENT: Resultant xy Force vs. Resultant xy Displacement'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Linear Displacement [mm]', fontsize=20)
        plt.ylabel('Force [N]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        resultant_xy_force = numpy.sqrt(forcetorque_values[2]**2 + forcetorque_values[0]**2)
        resultant_xy_enc = numpy.sqrt(lin_displacement_values_with_delay[2]**2 + lin_displacement_values_with_delay[0]**2)
        print("resultant_xy_force Length:", len(resultant_xy_force))
        index_second_start = extract_second_half_start_index(lin_displacement_values_with_delay[0])
        plt.plot(resultant_xy_enc[:index_second_start-1], resultant_xy_force[:index_second_start-1], color='pink')
        plt.plot(resultant_xy_enc[index_second_start:], resultant_xy_force[index_second_start:] * -1, color='purple')
        plt.legend(['Resultant xy Force vs. Resultant xy Displacement - 1st half', 'Resultant xy Force vs. Resultant xy Displacement - 2nd half'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 14e: LINEAR DISPLACEMENT: Resultant xy Displacement vs. time'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Time [sec]', fontsize=20)
        plt.ylabel('Linear Displacement [mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        resultant_xy_enc = numpy.sqrt(lin_displacement_values_with_delay[2] ** 2 + lin_displacement_values_with_delay[0] ** 2)
        plt.plot(resultant_xy_enc, color='black')
        plt.legend(['Resultant xy Displacement vs. time'], fontsize=20)
        plt.grid()

    if plot_Torques_vs_Displacement and len(forcetorque_values) and len(rot_displacement_values):
        fig = plt.figure('{} - Figure 15a: TORQUE vs. ROTARY DISPLACEMENT: Tx'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [°]', fontsize=20)
        plt.ylabel('Torques [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(rot_displacement_values_with_delay[0], forcetorque_values[3], color='blue')
        plt.legend(['Tx vs. Rotary Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 15b: TORQUE vs. ROTARY DISPLACEMENT: Ty'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [°]', fontsize=20)
        plt.ylabel('Torques [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.plot(rot_displacement_values_with_delay[0], forcetorque_values[4], color='orange')
        plt.legend(['Ty vs. Rotary Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 15c: TORQUE vs. ROTARY DISPLACEMENT: Tz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [°]', fontsize=20)
        plt.ylabel('Torques [N-mm]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        print("len of rot_displacement_values_with_delay", len(rot_displacement_values_with_delay[0]))
        print("len of forcetorque_values[5]", len(forcetorque_values[5]))
        plt.plot(rot_displacement_values_with_delay[0], forcetorque_values[5], color='green')
        plt.legend(['Tz vs. Rotary Displacement'], fontsize=20)
        plt.grid()

    '''
    For Haptics Testing - Gimbal Joints [N-m/rad], 
    Plot the Torque [N-mm] (column 5 from ForceTorque Array = torque about load cell Z-axis) VS Rotary Displacement [°], but convert N-mm/° to SSS units: N-m/rad
    '''
    if plot_Torques_vs_Displacement_Haptics and len(forcetorque_values) and len(rot_displacement_values):
        fig = plt.figure('{} - Figure 16a: TORQUE vs. ROTARY DISPLACEMENT: Tz'.format(folder), figsize=(10, 10))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [°]', fontsize=20)
        plt.ylabel('Torques [N-m]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        print("len of rot_displacement_values_with_delay", len(rot_displacement_values_with_delay[0]))
        print("len of forcetorque_values[5]", len(forcetorque_values[5]))
        plt.plot(rot_displacement_values_with_delay[0], forcetorque_values[5] / 1000, color='green')
        plt.legend(['Tz vs. Rotary Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 16b: TORQUE vs. ROTARY DISPLACEMENT: Tz'.format(folder), figsize=(10, 10))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [rad]', fontsize=20)
        plt.ylabel('Torques [N-m]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        print("len of rot_displacement_values_with_delay", len(rot_displacement_values_with_delay[0]))
        print("len of forcetorque_values[5]", len(forcetorque_values[5]))
        plt.plot(rot_displacement_values_with_delay[0] * math.pi / 180, forcetorque_values[5] / 1000, color='green')
        plt.legend(['Tz vs. Rotary Displacement'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 16c: SLOPE of STIFFNESS vs. ROTARY DISPLACEMENT [N-m/rad] vs. ROTARY DISPLACEMENT [°]: Tz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Rotary Displacement [rad]', fontsize=20)
        plt.ylabel('SLOPE of Stiffness vs Rotary Displacement [N-m/rad]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        print("len of rot_displacement_values_with_delay", len(rot_displacement_values_with_delay[0]))
        print("len of forcetorque_values[5]", len(forcetorque_values[5]))

        slope = []

        for i in range(len(forcetorque_values[5])):
            if i == 0:
                deltaTorque = forcetorque_values[5][i+1] - forcetorque_values[5][i]                        # Nmm
                deltaDisplacement = rot_displacement_values_with_delay[0][i+1] - rot_displacement_values_with_delay[0][i]     # deg
                if deltaDisplacement == 0:
                    slope.append(0.0)
                else:
                    slope.append((deltaTorque / deltaDisplacement) / 1000 * 180 / math.pi)  # Nm/rad
            else:
                deltaTorque = forcetorque_values[5][i] - forcetorque_values[5][i-1]                        # Nmm
                deltaDisplacement = rot_displacement_values_with_delay[0][i] - rot_displacement_values_with_delay[0][i-1]     # deg
                if deltaDisplacement == 0:
                    slope.append(0.0)
                else:
                    slope.append((deltaTorque / deltaDisplacement) / 1000 * 180 / math.pi)  # Nm/rad

        stiffness = (forcetorque_values[5] / rot_displacement_values_with_delay[0]) / 1000 * 180 / math.pi  # N-m/rad
        gradient = (numpy.gradient(forcetorque_values[5] / rot_displacement_values_with_delay[0])) / 1000 * 180 / math.pi   # N-m/rad

        # plt.plot(rot_displacement_values_with_delay[0], stiffness, color='red')
        # plt.plot(rot_displacement_values_with_delay[0], gradient, color='yellow')
        plt.plot(rot_displacement_values_with_delay[0] * math.pi / 180, slope, color='orange')
        plt.legend(['SLOPE of STIFFNESS vs. ROTARY DISPLACEMENT [N-m/rad] vs. ROTARY DISPLACEMENT [rad]'], fontsize=20)
        plt.grid()

    '''
    Plot the fft
    '''
    if plot_fft_all and len(xf):
        fig = plt.figure('{} - Figure 17: FFT'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.plot(xf, 2.0/N * numpy.abs(yf_2[0:N//2]), color='blue')
        plt.plot(xf, 2.0/N * numpy.abs(yf_0[0:N//2]), color='orange')
        plt.plot(xf, 2.0/N * numpy.abs(yf_1[0:N//2]), color='green')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_5[0:N//2]), color='blue')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_3[0:N//2]), color='orange')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_4[0:N//2]), color='green')
        plt.legend(['Fx fft', 'Fy fft', 'Fz fft'], fontsize=20)
        plt.grid()

    if plot_fft_components and len(xf):

        fig = plt.figure('{} - Figure 17a: FFT, Fx'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.plot(xf, 2.0/N * numpy.abs(yf_2[0:N//2]), color='blue')
        plt.legend(['Fx fft'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 17b: FFT, Fy'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.plot(xf, 2.0/N * numpy.abs(yf_0[0:N//2]), color='orange')
        plt.legend(['Fy fft'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 17c: FFT, Fz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.plot(xf, 2.0/N * numpy.abs(yf_1[0:N//2]), color='green')
        plt.legend(['Fz fft'], fontsize=20)
        plt.grid()

    if plot_fft_log_all and len(xf):

        fig = plt.figure('{} - Figure 18: FFT logarithmic xscale'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.xscale("log")
        plt.plot(xf, 2.0/N * numpy.abs(yf_2[0:N//2]), color='blue')
        plt.plot(xf, 2.0/N * numpy.abs(yf_0[0:N//2]), color='orange')
        plt.plot(xf, 2.0/N * numpy.abs(yf_1[0:N//2]), color='green')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_5[0:N//2]), color='blue')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_3[0:N//2]), color='orange')
        # plt.plot(xf, 2.0/N * numpy.abs(yf_4[0:N//2]), color='green')
        plt.legend(['Fx fft', 'Fy fft', 'Fz fft'], fontsize=20)
        plt.grid()

    if plot_fft_log_components and len(xf):

        fig = plt.figure('{} - Figure 18a: FFT logarithmic xscale, Fx'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=10)
        plt.xlabel('Frequency [Hz]', fontsize=10)
        plt.ylabel('Absolute Value?', fontsize=10)
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        N = len(forcetorque_values[0])
        plt.xscale("log")
        fft_abs_normalized_2 = 2.0/N * numpy.abs(yf_2[0:N//2])
        plt.plot(xf, fft_abs_normalized_2, color='blue')
        plt.legend(['Fx fft'], fontsize=10)
        plt.grid()

        fig = plt.figure('{} - Figure 18b: FFT logarithmic xscale, Fy'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.xscale("log")
        fft_abs_normalized_0 = 2.0/N * numpy.abs(yf_0[0:N//2])
        plt.plot(xf, fft_abs_normalized_0, color='orange')
        plt.legend(['Fy fft'], fontsize=20)
        plt.grid()

        fig = plt.figure('{} - Figure 18c: FFT logarithmic xscale, Fz'.format(folder))
        fig.suptitle(fig_suptitle, fontsize=20)
        plt.xlabel('Frequency [Hz]', fontsize=20)
        plt.ylabel('Amplitude [dB]', fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        N = len(forcetorque_values[0])
        plt.xscale("log")
        fft_abs_normalized_1 = 2.0/N * numpy.abs(yf_1[0:N//2])
        plt.plot(xf, fft_abs_normalized_1, color='green')
        plt.legend(['Fz fft'], fontsize=20)
        plt.grid()

    # Show all Figures at once by calling show() only once after all plot() functions are called
    plt.show()


def practice_plotting_sine(lin_displacement_values):
    fig = plt.figure('Sine')
    # fig.suptitle('Inputted Zaber Motor Parameters:\nsampling freq = {}\nrun time = {}'.format(sampling_freq, run_time), fontsize=12)
    plt.xlabel('Time [sec]')
    plt.ylabel('Linear Displacement [mm]')
    # plt.hlines(y=0, xmin=0, xmax=1.2772, linewidth=2, color='black')
    t = numpy.linspace(0, run_time, len(lin_displacement_values[0]))
    y = 25 * math.sin(numpy.multiply(2*math.pi*0.75, t))
    plt.plot(numpy.linspace(0, run_time, len(lin_displacement_values[0])), y)
    plt.legend(['ai0', 'ai1', 'ai2', 'ai3', 'ai4', 'ai5'])
    plt.grid()
    plt.show()

    # plt.hlines(y=0, xmin=0, xmax=1.2772, linewidth=2, color='black')
    # t = numpy.linspace(0, run_time, len(counter_data[0]))
    # y = 25 * math.sin(2*math.pi*0.75*t)
    # plt.plot(numpy.linspace(1.2772, run_time, len(counter_data[0])-1.2772), y)


def extract_second_half_start_index(data):
    count = 0
    prev_y = 0
    for index, y in enumerate(data):
        if y - prev_y > 0:
            count += 1
        if count > 5:
            break
        prev_y = y
    return index



def importData_filterData_plotData():
    """
    import_from_csv_config_parameters
        if import_config
            _Config_Parameters.csv
    import_from_csv_data
        if import_analog
            _Raw_Analog_Data.csv
            _Analog_Data_Filtered.csv
            _Analog_Data_Filtered_With_Offset.csv
            _Converted_To_Forces_Torques.csv
        if import_counter
            _Raw_Encoder_Data.csv
            _Converted_To_Linear_Displacement.csv
    plot_readings
        if plot_analog
            Figure 1: Analog Data RAW
            Figure 2: Analog Data FILTERED
            Figure 3: Analog Data FILTERED and after OFFSET
        if plot_forces
            Figure 4 : Analog Data converted to FORCES
            Figure 4a: Fx
            Figure 4b: Fy
            Figure 4c: Fz
        if plot_torques
            Figure 4 : Analog Data converted to TORQUES
            Figure 4d: Tx
            Figure 4e: Ty
            Figure 4f: Tz
        if not plot_torques
            Figure 5: Average Forces and Torques over entire {} sec run time
        if plot_encoder
            Figure 6: Linear Stages Encoder Data RAW
        if plot_displacement
            Figure 7: Linear Stages Encoder Data converted to LINEAR DISPLACEMENT
        if plot_ForcesTorquesDisplacement_vs_Time
            Figure 8: FORCES & TORQUES & LINEAR DISPLACEMENT vs. Time
        if plot_ForcesTorques_vs_Displacement
            Figure 9: FORCE & TORQUE vs. LINEAR DISPLACEMENT
    """
    import_config = True
    import_analog = True
    import_counter = True

    ''' Plot only the raw analog data from the load cell and only the raw encoder data from the linear gantry stages '''
    plot_analog = False
    plot_encoder = False
    ''' Plot Linear Gantry stuff'''
    plot_forces_all = False
    plot_forces_components = False
    plot_displacement_linear = False
    plot_ForcesDisplacement_vs_Time = False
    plot_Forces_vs_Displacement = False
    ''' Plot Rotary stuff'''
    plot_torques_all = False
    plot_torques_components = False
    plot_displacement_rotary = False
    plot_TorquesDisplacement_vs_Time = False
    plot_Torques_vs_Displacement = False
    plot_Torques_vs_Displacement_Haptics = True
    ''' Plot the arc displacement '''
    plot_displacement_arc = False
    ''' Plot the average forces / torques '''
    plot_avg_forces_torques = False
    ''' Plot FFT's (mostly just for noise characterization of gantry linear stages) '''
    plot_fft_all = False
    plot_fft_components = False
    plot_fft_log_all = False
    plot_fft_log_components = False

    folder_of_data = get_folder_name_from_user()
    current_directory = get_curr_dir_for_importing()

    load_cell_sn, cDAQ9189_ai9252_module, sampling_freq, run_time, number_of_samples, reuse_offsets, use_lowPass_filter, filter_cutoff_freq, running_mean_window_size, \
    notes, usb_com_port, action_to_execute, gantry_sideways_clamp, \
    gantry_sin_amplitude, gantry_sin_frequency, gantry_sin_cycles, gantry_const_vel_motion_profile, gantry_const_vel_relative_distance, gantry_const_vel_speed, gantry_const_vel_cycles, \
    rotary_sin_amplitude, rotary_sin_frequency, rotary_sin_cycles, rotary_const_vel_motion_profile, rotary_const_vel_relative_angle, rotary_const_vel_speed, rotary_const_vel_cycles = import_from_csv_config_parameters(folder_of_data, current_directory, import_config)

    loadcell_data_raw_csv, loadcell_data_filtered_csv, loadcell_data_filtered_with_offset_csv, forcetorque_values_csv, encoder_data_csv, lin_displacement_values_csv, rot_displacement_values_csv, angular_displacement_values_csv, time_stamps = import_from_csv_data(folder_of_data, current_directory, import_analog, import_counter)

    # convert_counter_to_lin_displacement()
    # convert_analog_to_forces_torques()
    # calc_velocity()

    force_torque_values_csv_avg = print_average_force_torque(forcetorque_values_csv)

    xf, yf_0, yf_1, yf_2, yf_3, yf_4, yf_5 = calc_fft(forcetorque_values_csv, sampling_freq)

    plot_readings(folder_of_data,
                  sampling_freq, run_time, number_of_samples, filter_cutoff_freq, running_mean_window_size, action_to_execute, gantry_sin_amplitude, gantry_sin_frequency, gantry_sin_cycles, gantry_const_vel_relative_distance, gantry_const_vel_speed, gantry_const_vel_cycles,
                  rotary_sin_amplitude, rotary_sin_frequency, rotary_sin_cycles, rotary_const_vel_relative_angle, rotary_const_vel_speed, rotary_const_vel_cycles,
                  loadcell_data_raw_csv, loadcell_data_filtered_csv, loadcell_data_filtered_with_offset_csv, forcetorque_values_csv, force_torque_values_csv_avg, encoder_data_csv, lin_displacement_values_csv, rot_displacement_values_csv, angular_displacement_values_csv, time_stamps,
                  xf, yf_0, yf_1, yf_2, yf_3, yf_4, yf_5,
                  plot_analog, plot_forces_all, plot_forces_components, plot_torques_all, plot_torques_components, plot_avg_forces_torques, plot_encoder, plot_displacement_linear, plot_displacement_rotary, plot_displacement_arc, plot_ForcesDisplacement_vs_Time, plot_TorquesDisplacement_vs_Time, plot_Forces_vs_Displacement, plot_Torques_vs_Displacement, plot_Torques_vs_Displacement_Haptics, plot_fft_all, plot_fft_components, plot_fft_log_all, plot_fft_log_components)

    # practice_plotting_sine(lin_displacement_values_csv)

    # loadcell_data, loadcell_data_with_offset, forcetorque_values, encoder_data, lin_displacement_values = import_from_xlsx(plot_analog, plot_digital)

if __name__ == '__main__':
    importData_filterData_plotData()
### Introduction ###
This folder contains the Python scripts for controlling the 1-axis Zaber motorized Rotary Stage Fixture (on the 3-axis Gantry Fixture) and simultaneously reading the DAQ device, which collects both load cell data (6 DOF: Fx, Fy, Fz, Tx, Ty, Tz) as well as encoder displacement data (from Zaber rotary stage). NOTE! These scripts are to be run from a Windows computer. 

### How to run the program? ###
1. Simultaneously command Zaber stage & collect DAQ data (load cell data & Zaber stage's encoder data):
```py .\BOTH_DataAQ_Zaber.py```
2. Collect DAQ data ONLY:
```py .\DataAq_Functions_Main.py ```
3. Command Zaber ONLY:
```py .\Zaber_Main.py --action <action_you_want_to_execute>``` where `action_you_want_to_execute` is one of the following options:
   1. `gantry_home`
   2. `gantry_center`
   3. `gantry_move_relative` 
   4. `gantry_move_absolute` 
   5. `gantry_move_absolute_hid_origin` 
   6. `gantry_trace_arc_abs_origin` 
   7. `gantry_trace_arc_rel_origin` 
   8. `gantry_interaction_force` 
   9. `gantry_force_to_start`
   10. `rotary_home` 
   11. `rotary_move_absolute` 
   12. `rotary_move_relative` 
   13. `rotary_interaction_torque` 
   14. `rotary_torque_to_start`

### Configs ###
Currently, the config file where the user would specify the test parameters (e.g. Zaber stage's sine wave amplitude and frequency) is named `config.ini`. Eventually, 
Configuration files will be located in the folder named `\Trajectory_Config_Files`. Config files are `.ini` type files.
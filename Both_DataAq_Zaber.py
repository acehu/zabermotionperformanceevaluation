import time
from threading import Thread

from DataAq_Functions_Main import acquire_data, get_load_cell_offsets, connect_to_daq
# from DataAq_Functions_Main_cDAQ_9185 import acquire_data, get_load_cell_offsets, connect_to_daq

from DAQ_Offsets import DAQ_Offsets
from Zaber_Main import Zaber_motion
from Helper import system_check, make_dir_for_exporting
from Gantry_config import Gantry_config

Config = Gantry_config('config.ini')


'''
THE REAL PROGRAM STARTS FROM HERE!!!!!
'''

argparse_action = None

date_time, newpath_daq_data, newpath_daq_data_newest, newpath_zaber_log, newpath_zaber_log_pos = make_dir_for_exporting()

offsets = DAQ_Offsets()
if Config.reuse_offsets:
    system_check(offsets.read_offset_file())
else:
    offset_values = get_load_cell_offsets(Config)
    system_check(offsets.save_offset_file(offset_values))
offset_values = offsets.values

print("\n ~~~ Create DAQ Thread ~~~ ")
thread1 = Thread(target=acquire_data, args=(Config, offset_values, date_time, newpath_daq_data, newpath_daq_data_newest))
print("\n ~~~ Create ZABER Thread ~~~ ")
thread2 = Thread(target=Zaber_motion, args=(Config, argparse_action, date_time, newpath_zaber_log, newpath_zaber_log_pos))

thread2.daemon = True

print("\n ~~~ Start DAQ Thread ~~~ ")
thread1.start()
print("\n ~~~ Start ZABER Thread ~~~ ")
thread2.start()

print("\n ~~~ Join DAQ Thread ~~~ ")
thread1.join()
time.sleep(3)
print("\n ~~~ Join ZABER Thread ~~~ ")
thread2.join()

import time
import os
import argparse
from datetime import datetime
from zaber_motion import Units
from zaber_motion import Library
from zaber_motion import LogOutputMode
from zaber_motion import RotationDirection
from zaber_motion.ascii import Connection
from zaber_motion.ascii import Warnings

import Zaber_Functions
import Helper
from Gantry_config import Gantry_config, read_points


rotation_direction = RotationDirection.CCW
counter_rotation_direction = RotationDirection.CW


def Zaber_motion(Config, argparse_action, date_time, newpath_zaber_log, newpath_zaber_log_pos):

    if argparse_action is None:
        com_port = Config.usb_com_port
        action_to_execute = Config.action_to_execute
    else:
        action_to_execute = argparse_action
        if 'gantry' in action_to_execute:
            com_port = 'COM4'
        elif 'rotary' in action_to_execute:
            com_port = 'COM8'

    zaber_config = Config.zaber_config
    test_system, point_to_test, gantry_X, gantry_Y, gantry_Z = read_points('point_lists.ini')
    point_to_test = point_to_test + 1

    if not os.path.exists(r'Zaber_Log'):
        os.makedirs(r'Zaber_Log')

    # Library.set_log_output(LogOutputMode.STDOUT)
    Library.set_log_output(LogOutputMode.FILE, newpath_zaber_log)
    Library.enable_device_db_store()

    with Connection.open_serial_port(com_port) as connection:
        device_list = connection.detect_devices()
        print("\nFound {} devices".format(len(device_list)))
        device = device_list[0]
        print("Number of axes on this device: {}".format(device.axis_count))

        """
        LINEAR STAGES FUNCTIONS
        """
        if Config.reuse_offsets:
            time.sleep(4)

        if com_port == 'COM4':
        # if device.axis_count == 4:

            axis1 = device.get_axis(1)
            axis2 = device.get_axis(2)
            axis3 = device.get_axis(3)
            axis4 = device.get_axis(4)

            print("Axis4 Peripheral ID:", axis4.identity.peripheral_id)
            print("Axis4 Peripheral Name:", axis4.identity.peripheral_name)

            ' Read max current (can only read max current) '
            Zaber_Functions.get_max_current_linear(device)  # Units currently in DC
            ' Set & Read run current. For all 4 linear stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK (2.256 AC_PEAK according to Zaber engr) / Min = 65 NATIVE or 1.3 AC_PEAK. Set z-axis to 90 NATIVE or 1.80 AC_PEAK to counter gravity '
            # Zaber_Functions.set_run_current_linear(device, run_current_axis1=2.256, run_current_axis3=2.256, run_current_axis4=2.256)  # Units currently in AC_PEAK
            Zaber_Functions.get_run_current_linear(device)
            ' Set & Read hold current. For x1, x2, y3 stages: default = 80 NATIVE. For z4-stage: default = 120 NATIVE. Set to 0 NATIVE to backdrive. '
            # Zaber_Functions.set_hold_current_linear(device, hold_current_axis1=80.0, hold_current_axis3=80.0, hold_current_axis4=120.0)  # Units currently in NATIVE
            Zaber_Functions.get_hold_current_linear(device)

            'Read initial position'
            print("\nINITIAL GANTRY COORDINATES WRT GANTRY ORIGIN: ")
            print("get_pos_1: ", axis1.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis1.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_2: ", axis2.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis2.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_3: ", axis3.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis3.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_4: ", axis4.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis4.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)

            print("\nINITIAL GANTRY COORDINATES WRT HID ORIGIN: ")
            print("get_pos_1: ", (723.95+12.63+4.30)-axis3.get_position(Units.LENGTH_MILLIMETRES), " mm")
            print("get_pos_2: ", axis1.get_position(Units.LENGTH_MILLIMETRES)-(503.046991-10.53), " mm")
            print("get_pos_3: ", axis2.get_position(Units.LENGTH_MILLIMETRES)-(503.046991-10.53), " mm")
            print("get_pos_4: ", axis4.get_position(Units.LENGTH_MILLIMETRES)-(645.555443-8.07-0.15), " mm")

            print("\n\n *** Commencing commanded motion *** \n")

            'LINEAR: Home gantry stages'
            if action_to_execute == 'gantry_home':
                Zaber_Functions.home_all_axes_linear(device)

            'LINEAR: Move gantry to gantry center (x=500, y=375, z=375)'
            if action_to_execute == 'gantry_center':
                Zaber_Functions.move_abs(device, x=500, y=375, z=375, speed=75, units='mm')  # Units: x, y, z = mm. speed = mm/s. Travel Ranges: x = 1000 mm. y, z = 750 mm

            'LINEAR: Move each gantry stage by a relative displacement'
            if action_to_execute == 'gantry_move_relative':
                Zaber_Functions.move_rel(device, x=zaber_config.gantry_X_mm_steps, y=zaber_config.gantry_Y_mm_steps, z=zaber_config.gantry_Z_mm_steps, speed=zaber_config.gantry_speed_mm_s)  # Units: x, y, z = mm. speed = mm/s. Travel Ranges: x = 1000 mm. y, z = 750 mm

            'LINEAR: Move gantry to gantry coordinate (x, y, z)'
            if action_to_execute == 'gantry_move_absolute':
                Zaber_Functions.stop_all_stages(com_port=Config.usb_com_port)
                device.warnings.clear_flags()
                Zaber_Functions.move_abs(device, x=zaber_config.gantry_X_mm_steps, y=zaber_config.gantry_Y_mm_steps, z=zaber_config.gantry_Z_mm_steps, units=zaber_config.gantry_units, speed=zaber_config.gantry_speed_mm_s)  # Units: x, y, z = mm. speed = mm/s. Travel Ranges: x = 1000 mm. y, z = 750 mm

            'LINEAR: Move gantry to gantry coordinate (x, y, z) wrt HID origin'
            if action_to_execute == 'gantry_move_absolute_hid_origin':
                Zaber_Functions.move_abs_wrt_hid_origin(device, x=zaber_config.gantry_X_mm_steps, y=zaber_config.gantry_Y_mm_steps, z=zaber_config.gantry_Z_mm_steps, speed=zaber_config.gantry_speed_mm_s, sideways=zaber_config.gantry_sideways_clamp)

                if 'arc' in test_system and test_system != 'arc_G0':

                    print("Adjusting start point so that it falls on the arc path")
                    time.sleep(1)

                    # corrected_position_uStep = 0.9629 * axis3.get_position(unit=Units.NATIVE) - 3203.9
                    # print("corrected", corrected_position_uStep)
                    # axis3.move_absolute(position=corrected_position_uStep, unit=Units.NATIVE)

                    if test_system == 'arc_G0_smaller' and point_to_test == 1:
                        axis3.move_absolute(position=116063, unit=Units.NATIVE)
                    elif test_system == 'arc_G0_smaller' and point_to_test == 2:
                        axis3.move_absolute(position=151318, unit=Units.NATIVE)
                    elif test_system == 'arc_G0_smaller' and point_to_test == 3:
                        axis3.move_absolute(position=196601, unit=Units.NATIVE)

                    elif test_system == 'arc_G1' and point_to_test == 1:
                        axis3.move_absolute(position=176519, unit=Units.NATIVE)
                        # axis4.move_absolute(position=354332, unit=Units.NATIVE)
                    elif test_system == 'arc_G1' and point_to_test == 2:
                        axis3.move_absolute(position=117850, unit=Units.NATIVE)
                        # axis4.move_absolute(position=343947, unit=Units.NATIVE)
                    elif test_system == 'arc_G1' and point_to_test == 3:
                        axis3.move_absolute(position=62878, unit=Units.NATIVE)
                    # TROUBLESHOOTING ARC MOTION!!!
                    elif test_system == 'arc_G1' and point_to_test == 4:
                        axis3.move_absolute(position=198440, unit=Units.NATIVE)
                        print("Done")
                    # TROUBLESHOOTING ARC MOTION!!!

                    elif test_system == 'arc_G1_smaller' and point_to_test == 1:
                        axis3.move_absolute(position=164420, unit=Units.NATIVE)
                    elif test_system == 'arc_G1_smaller' and point_to_test == 2:
                        axis3.move_absolute(position=114200, unit=Units.NATIVE)
                    elif test_system == 'arc_G1_smaller' and point_to_test == 3:
                        axis3.move_absolute(position=64010, unit=Units.NATIVE)

                    elif test_system == 'arc_G2' and point_to_test == 1:
                        axis3.move_absolute(position=167140, unit=Units.NATIVE)
                    elif test_system == 'arc_G2' and point_to_test == 2:
                        axis3.move_absolute(position=217110, unit=Units.NATIVE)
                    elif test_system == 'arc_G2' and point_to_test == 3:
                        axis3.move_absolute(position=244230, unit=Units.NATIVE)

                    elif test_system == 'arc_G2_smaller' and point_to_test == 1:
                        axis3.move_absolute(position=117900, unit=Units.NATIVE)
                    elif test_system == 'arc_G2_smaller' and point_to_test == 2:
                        axis3.move_absolute(position=173125, unit=Units.NATIVE)
                    elif test_system == 'arc_G2_smaller' and point_to_test == 3:
                        axis3.move_absolute(position=237400, unit=Units.NATIVE)

            'LINEAR: X-stage sine motion'
            if action_to_execute == 'gantry_x_sin':
                Zaber_Functions.do_x_sin(device, amplitude=zaber_config.interaction_amplitude_mm_deg, frequency=zaber_config.interaction_frequency_hz, cycles=zaber_config.interaction_cycles)  # Units: amp = mm. freq = Hz
            'LINEAR: Y-stage sine motion'
            if action_to_execute == 'gantry_y_sin':
                Zaber_Functions.do_y_sin(device, amplitude=zaber_config.interaction_amplitude_mm_deg, frequency=zaber_config.interaction_frequency_hz, cycles=zaber_config.interaction_cycles)  # Units: amp = mm. freq = Hz
            'LINEAR: Z-stage sine motion'
            if action_to_execute == 'gantry_z_sin':
                Zaber_Functions.do_z_sin(device, amplitude=zaber_config.interaction_amplitude_mm_deg, frequency=zaber_config.interaction_frequency_hz, cycles=zaber_config.interaction_cycles)  # Units: amp = mm. freq = Hz

            'LINEAR: Force to Start/Stiction Test'
            if action_to_execute == 'gantry_force_to_start':
                Zaber_Functions.do_force_to_move(device, trajectory=zaber_config.friction_motion_profile, distance=zaber_config.friction_PeakToPeak_distance_mm_deg, speed=zaber_config.friction_speed_mm_deg_sec, cycles=zaber_config.friction_cycles)

            'LINEAR: Interaction Force Test'
            if action_to_execute == 'gantry_interaction_force':
                Zaber_Functions.do_interaction_force(device, amplitude=zaber_config.interaction_amplitude_mm_deg, frequency=zaber_config.interaction_frequency_hz, cycles=zaber_config.interaction_cycles)  # Units: amp = mm. freq = Hz

            'LINEAR: Trace an arc'
            if action_to_execute == 'gantry_trace_arc_abs_origin':

                """ 
                Note: Include the call to stop before and after arc motion because everytime an arc is run, Zaber always complains about "Stream Discontinuity":                
                 - Terminal: "zaber_motion.exceptions.stream_setup_failed_exception.StreamSetupFailedException: StreamSetupFailedException: Stream setup failed. Ensure that the axes and the stream number are correct."
                 - Zaber Launcher: "The axis has slowed down while following a streamed motion path because it has run out of queued motions. This warning persists until the stream has enough motions queued that it no longer needs to decelerate for that reason, or until the stream is disabled."
                """
                Zaber_Functions.stop_all_stages(Config.usb_com_port)

                ' Plane to execute arc in '
                if 'G0' in test_system:
                    target_axes_indices = [1, 1, 0]  # x, y plane (tests G0)
                elif 'G1' in test_system or 'G2' in test_system:
                    target_axes_indices = [1, 0, 1]  # x, z plane (tests G1, G2)

                ' Center of arc '
                if 'G0' in test_system:
                    arc_circle_center_abs = (0, 0, -142.00)          # arc_circle_center_abs_arc_G0
                # TROUBLESHOOTING ARC MOTION!!!
                elif 'G1' in test_system and point_to_test == 4:
                    print("Here", test_system, point_to_test)
                    arc_circle_center_abs = (0+70.4, 0, -192.00)  # 0+70.4+7.8
                # TROUBLESHOOTING ARC MOTION!!!
                elif 'G1' in test_system:
                    arc_circle_center_abs = (0, 0, -117.00)          # arc_circle_center_abs_arc_G1
                elif 'G2' in test_system and point_to_test == 1:
                    arc_circle_center_abs = (279.0883194, 0, -332)   # arc_circle_center_abs_arc_G2_PathO1
                elif 'G2' in test_system and point_to_test == 2:
                    arc_circle_center_abs = (167.3776867, 0, -427)   # arc_circle_center_abs_arc_G2_PathO2
                elif 'G2' in test_system and point_to_test == 3:
                    arc_circle_center_abs = (-40.19067056, 0, -467)  # arc_circle_center_abs_arc_G2_PathO3

                arc_start_abs = (zaber_config.x_start, zaber_config.y_start, zaber_config.z_start)
                arc_end_abs = (zaber_config.x_end, zaber_config.y_end, zaber_config.z_end)
                start_point = Zaber_Functions.transform_origins(arc_start_abs, True)
                Zaber_Functions.gantry_trace_arc_abs_origin(device,
                                                            target_axes_indices,
                                                            rotation_direction,
                                                            counter_rotation_direction,
                                                            arc_circle_center_abs,
                                                            arc_start_abs,
                                                            arc_end_abs,
                                                            speed=zaber_config.arc_speed_mm_s, cycles=zaber_config.arc_cycles)

                Zaber_Functions.stop_all_stages(Config.usb_com_port)

            'LINEAR: Trace an arc in relative'
            if action_to_execute == 'gantry_trace_arc_rel_origin':

                ' Plane to execute arc in '
                target_axes_indices_xy = [1, 1, 0]  # x, y plane (tests G0)
                target_axes_indices_xz = [1, 0, 1]  # x, z plane (tests G1, G2)
                target_axes_indices_yz = [0, 1, 1]  # y, z plane
                target_axes_indices = target_axes_indices_xy

                ' Center of arc '
                arc_circle_center_rel_G0 = (0,0,-100)
                arc_circle_center_rel_G1 = (0,0,-100)
                arc_circle_center_rel_G2 = (100,0,-100)
                arc_circle_center_rel = arc_circle_center_rel_G0

                current_position_xx_stages = axis1.get_position(Units.LENGTH_MILLIMETRES)
                current_position_y_stage = axis3.get_position(Units.LENGTH_MILLIMETRES)
                current_position_z_stage = axis4.get_position(Units.LENGTH_MILLIMETRES)
                arc_start_rel = (current_position_xx_stages, current_position_y_stage, current_position_z_stage)
                arc_end_rel = (zaber_config.x_end, zaber_config.y_end, zaber_config.z_end)
                start_point = Zaber_Functions.transform_origins(arc_start_rel, True)
                Zaber_Functions.gantry_trace_arc_rel_origin(device,
                                                            target_axes_indices,
                                                            rotation_direction,
                                                            counter_rotation_direction,
                                                            arc_circle_center_rel,
                                                            arc_start_rel,
                                                            arc_end_rel,
                                                            speed=15, cycles=1)

            print("\n *** Completed commanded motion *** \n")

            'Read final position'
            print("FINAL GANTRY COORDINATES WRT GANTRY ORIGIN: ")
            print("get_pos_1: ", axis1.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis1.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_2: ", axis2.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis2.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_3: ", axis3.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis3.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)
            print("get_pos_4: ", axis4.get_position(Units.LENGTH_MILLIMETRES), " mm = ", axis4.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)

            print("\nFINAL GANTRY COORDINATES WRT HID ORIGIN: ")
            print("get_pos_1: ", (723.95+12.63+4.30)-axis3.get_position(Units.LENGTH_MILLIMETRES), " mm")
            print("get_pos_2: ", axis1.get_position(Units.LENGTH_MILLIMETRES)-(503.046991-10.53), " mm")
            print("get_pos_3: ", axis2.get_position(Units.LENGTH_MILLIMETRES)-(503.046991-10.53), " mm")
            print("get_pos_4: ", axis4.get_position(Units.LENGTH_MILLIMETRES)-(645.555443-8.07-0.15), " mm")

            'Read Resolution'
            'Do NOT set resolution every run! Will twist the motors!'
            # axis1.settings.set(setting="resolution", value=64)
            # axis2.settings.set(setting="resolution", value=64)
            # axis3.settings.set(setting="resolution", value=64)
            # axis4.settings.set(setting="resolution", value=64)
            print("\nResolution of Motors set to (Do NOT set resolution every run! Will twist the motors!): ")
            print("axis1.settings.get encoder.resolution = {}".format(axis1.settings.get(setting="resolution")))  # Default = 64.0
            print("axis2.settings.get encoder.resolution = {}".format(axis2.settings.get(setting="resolution")))  # Default = 64.0
            print("axis3.settings.get encoder.resolution = {}".format(axis3.settings.get(setting="resolution")))  # Default = 64.0
            print("axis4.settings.get encoder.resolution = {}".format(axis4.settings.get(setting="resolution")))  # Default = 64.0
            print()

        """
        ROTARY STAGE FUNCTIONS
        """

        if com_port == 'COM8':
        # if device.axis_count == 1 or device.axis_count == 2:

            axis_rotary = device.get_axis(1)

            ' Read max current '
            Zaber_Functions.get_max_current_rotary(device)
            ' Set & Read run current. For all 3 stages: Max (default) = 160 NATIVE or 3.2 AC_PEAK / Min = 65 NATIVE or 1.3 AC_PEAK. Set z-axis to 90 NATIVE to counter gravity '
            # Zaber_Functions.set_run_current_rotary(device, run_current_rotary=0.56)
            Zaber_Functions.get_run_current_rotary(device)
            ' Set & Read hold current. For x1, x2, y3 stages: default = 80 NATIVE. For z4-stage: default = 120 NATIVE. Set to 0 NATIVE to backdrive. '
            # Zaber_Functions.set_hold_current_rotary(device, hold_current_rotary=25.0)
            Zaber_Functions.get_hold_current_rotary(device)

            'Read initial position'
            print("\nINITIAL ROTARY STAGE ANGLE: ")
            print("get_pos_1: ", axis_rotary.get_position(Units.ANGLE_DEGREES), "° = ", axis_rotary.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)

            print("\n *** Commencing commanded motion *** \n")

            'ROTARY: Home rotary stage'
            if action_to_execute == 'rotary_home':
                Zaber_Functions.home_all_axes_rotary(device)

            'ROTARY: Move rotary stage to absolute angle'
            if action_to_execute == 'rotary_move_absolute':
                Zaber_Functions.rotate_to_abs(device, angle=zaber_config.rotary_angle_deg, speed=zaber_config.rotary_speed_deg_s)

            'ROTARY: Move rotary stage by a relative angular displacement'
            if action_to_execute == 'rotary_move_relative':
                Zaber_Functions.rotate_to_rel(device, angle=zaber_config.rotary_angle_deg, speed=zaber_config.rotary_speed_deg_s)

            'ROTARY: Torque to Start/Stiction Test'
            if action_to_execute == 'rotary_torque_to_start':
                Zaber_Functions.do_torque_to_move(device, trajectory=zaber_config.friction_motion_profile, start_point=zaber_config.friction_start_point, angle=zaber_config.friction_PeakToPeak_distance_mm_deg, speed=zaber_config.friction_speed_mm_deg_sec, cycles=zaber_config.friction_cycles)

            'ROTARY: Interaction Torque Test'
            if action_to_execute == 'rotary_interaction_torque':
                Zaber_Functions.rotate_sin(device, amplitude=zaber_config.interaction_amplitude_mm_deg, frequency=zaber_config.interaction_frequency_hz, cycles=zaber_config.interaction_cycles)

            print("\n *** Completed commanded motion *** \n")

            'Read final position'
            print("FINAL ROTARY STAGE ANGLE: ")
            print("get_pos_1: ", axis_rotary.get_position(Units.ANGLE_DEGREES), "° = ", axis_rotary.get_position(Units.NATIVE), " microsteps (aka Native)")  # get_position Returns current axis position wrt gantry origin (i.e. 500, 375, 375 is center of gantry)

            'Read Resolution'
            'Do NOT set resolution every run! Will twist the motors!'
            print("\nResolution of Motors set to (Do NOT set resolution every run! Will twist the motors!): ")
            print("axis_rotary.settings.get encoder.resolution = {}".format(axis_rotary.settings.get(setting="resolution")))  # Default = 64.0
            print()

        """
        PRINT WARNINGS
        CLEAR WARNINGS
        """
        '''
        If E-stop was pushed, re-enable the stages by going to the Zaber Console Terminal and entering: driver enable
        If a stage was disconnected and re-connected, activate the stage by going to the Zaber Console Terminal and entering: activate
        '''

        if action_to_execute == 'warnings_print':
            Zaber_Functions.warnings_print(device)

        if action_to_execute == 'warnings_clear':
            Zaber_Functions.warnings_clear(device)



if __name__ == '__main__':

    Config = Gantry_config('config.ini')

    parser = argparse.ArgumentParser(description='Run Zaber_Main.py to command Zaber gantry to home or go to center Syntax: \n'
                                                 'py .\\Zaber_Main.py --action gantry_home OR gantry_center')
    parser.add_argument('--action', type=str, dest='action_to_execute', help='Specify motion to perform! Options: gantry_home, gantry_center')
    args = parser.parse_args()
    argparse_action = args.action_to_execute

    date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    ''' Directory for Zaber_Log'''
    newpath_zaber_log = r'Zaber_Log/{}_motion_library_log.txt'.format(date_time)
    newpath_zaber_log_pos = r'Zaber_Log/{}_exampleFile_pos_track.txt'.format(date_time)

    Zaber_motion(Config, argparse_action, date_time, newpath_zaber_log, newpath_zaber_log_pos)

""" Hello. This script is for acquiring data from a DAQ... """


""" (A) Import API's """
import time
import os
import numpy
import nidaqmx
import xlsxwriter  # For exporting to xlsx
import pandas  # For importing from xlsx
from datetime import datetime
from nidaqmx.stream_readers import CounterReader
from nidaqmx.stream_readers import AnalogMultiChannelReader
from nidaqmx.constants import EncoderType

import Constants
import Zaber_Functions
from Helper import lowpass_filter, notch_filter, convert_analog_to_forces_torques, convert_counter_to_lin_displacement
from Gantry_config import Gantry_config, read_points

Config = Gantry_config('config.ini')

if Config.load_cell_sn == 35009:
    cDAQ9185_ai9252_module = 'cDAQ9185-1DAC5EBMod6'
    cDAQ9185_ai9252_module_ai_0 = 'cDAQ9185-1DAC5EBMod6/ai0'
    cDAQ9185_ai9252_module_ai_1 = 'cDAQ9185-1DAC5EBMod6/ai1'
    cDAQ9185_ai9252_module_ai_2 = 'cDAQ9185-1DAC5EBMod6/ai2'
    cDAQ9185_ai9252_module_ai_3 = 'cDAQ9185-1DAC5EBMod6/ai3'
    cDAQ9185_ai9252_module_ai_4 = 'cDAQ9185-1DAC5EBMod6/ai4'
    cDAQ9185_ai9252_module_ai_5 = 'cDAQ9185-1DAC5EBMod6/ai5'
if Config.load_cell_sn == 35011:
    cDAQ9185_ai9252_module = 'cDAQ9185-1DAC5EBMod7'
    cDAQ9185_ai9252_module_ai_0 = 'cDAQ9185-1DAC5EBMod7/ai0'
    cDAQ9185_ai9252_module_ai_1 = 'cDAQ9185-1DAC5EBMod7/ai1'
    cDAQ9185_ai9252_module_ai_2 = 'cDAQ9185-1DAC5EBMod7/ai2'
    cDAQ9185_ai9252_module_ai_3 = 'cDAQ9185-1DAC5EBMod7/ai3'
    cDAQ9185_ai9252_module_ai_4 = 'cDAQ9185-1DAC5EBMod7/ai4'
    cDAQ9185_ai9252_module_ai_5 = 'cDAQ9185-1DAC5EBMod7/ai5'
if Config.load_cell_sn == 35010 or Config.load_cell_sn == 38394 or Config.load_cell_sn == 38395:
    cDAQ9185_ai9252_module = 'cDAQ9185-1DAC5EBMod4'
    cDAQ9185_ai9252_module_ai_0 = 'cDAQ9185-1DAC5EBMod4/ai0'
    cDAQ9185_ai9252_module_ai_1 = 'cDAQ9185-1DAC5EBMod4/ai1'
    cDAQ9185_ai9252_module_ai_2 = 'cDAQ9185-1DAC5EBMod4/ai2'
    cDAQ9185_ai9252_module_ai_3 = 'cDAQ9185-1DAC5EBMod4/ai3'
    cDAQ9185_ai9252_module_ai_4 = 'cDAQ9185-1DAC5EBMod4/ai4'
    cDAQ9185_ai9252_module_ai_5 = 'cDAQ9185-1DAC5EBMod4/ai5'

print("Load Cell SN: {}".format(Config.load_cell_sn))
print("cDAQ9185_ai9252_module: {}".format(cDAQ9185_ai9252_module))



""" (B) Function Definitions """

def connect_to_daq():
    """
    Connect to DAQ
    :return: None
    """
    print("\n *** Begin connecting to DAQ ***")
    # Initialize cDAQ
    system = nidaqmx.system.System.local()
    # Print the model of each DAQ-module (Analog and Counter DAQ cards) in the cDAQ
    for device in system.devices:
        print("Device: ", device)
    # Initialize each DAQ-module in the cDAQ. Not required for the Counter DAQ.
    device_9252 = system.devices[cDAQ9185_ai9252_module]  # NI-9252 = Analog DAQ
    device_9361 = system.devices['cDAQ9185-1DAC5EBMod1']  # NI-9361 = Counter DAQ
    # Declare each channel in the Analog DAQ-module; 2 ways of doing this are shown below. Not required for the Counter DAQ.
    phys_chan_0 = device_9252.ai_physical_chans['ai0']
    phys_chan_1 = device_9252.ai_physical_chans['ai1']
    phys_chan_2 = device_9252.ai_physical_chans['ai2']
    phys_chan_3 = nidaqmx.system.PhysicalChannel('cDAQ9185-1DAC5EBMod4/ai3')
    phys_chan_4 = nidaqmx.system.PhysicalChannel('cDAQ9185-1DAC5EBMod4/ai4')
    phys_chan_5 = nidaqmx.system.PhysicalChannel('cDAQ9185-1DAC5EBMod4/ai5')
    print("*** Successfully connected to DAQ *** \n")


def get_load_cell_offsets(Config):
    """
    Obtain load cell offsets
    :parameter rate:
    :parameter number_of_samples_per_channel:
    :parameter filter_type:
    :parameter cutoff:
    :parameter running_mean_window_size:
    :return:
    """

    rate = Config.sampling_freq
    number_of_samples_per_channel = Config.number_of_samples
    cutoff = Config.filter_cutoff_freq
    running_mean_window_size = Config.running_mean_window_size

    manual_filter = None
    if Config.use_lowPass_filter:
        manual_filter = Constants.lowPass_filter
        # manual_filter = Constants.notch_filter
    filter_type = manual_filter

    print("\n *** Begin getting load cell offset values ***")

    # Set up Analog Task to acquire Analog Data from respective Analog DAQ
    with nidaqmx.Task() as analog_task, nidaqmx.Task() as counter_task:
        # Initialize each channel in Analog DAQ module
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_0, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_1, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_2, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_3, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_4, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_5, max_val=0.1, min_val=-0.1)

        # Set up clock for Analog DAQ
        analog_task.timing.cfg_samp_clk_timing(rate=Config.sampling_freq, samps_per_chan=5000, sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS)
        analog_offset_reader = AnalogMultiChannelReader(analog_task.in_stream)
        analog_task.start()

        print("Obtaining load cell offset values for {} Samples \n".format(5000))

        # analog data shape (n,6)
        load_cell_offset_values = numpy.zeros((6, 5000))
        analog_offset_reader.read_many_sample(load_cell_offset_values, 5000, nidaqmx.constants.WAIT_INFINITELY)

        load_cell_offset_values = numpy.transpose(load_cell_offset_values)

    if filter_type == Constants.lowPass_filter:
        load_cell_offset_values = lowpass_filter(load_cell_offset_values, 500, rate)
    elif filter_type == Constants.Notch_filter:
        load_cell_offset_values = notch_filter(load_cell_offset_values, cutoff, rate)

    load_cell_offset_values = numpy.mean(load_cell_offset_values, axis=0)

    print("Offset values obtained. Raw Analog Data Offset Values: \n", load_cell_offset_values, "\n")
    # input("You can load weights now! Once weights are loaded, press Enter to continue... \n")

    return load_cell_offset_values


def collect_analog_counter_readings(rate, number_of_samples_per_channel, offset):
    """
    Collect analog input readings (load cell force & torque readings) && counter input readings (Zaber motor's x1,
    x2, y3, z4 encoder readings)
    :parameter:
    :return: analog readings 2D array
    :return: counter readings 2D array
    """

    print("\n *** Sampling Rate: {} samples/sec *** \n".format(rate))

    if Config.run_time > 60:
        run_time_minutes, run_time_seconds = divmod(Config.run_time, 60)
        print("\n *** Start collecting raw load cell and raw encoder data for {} seconds ({} minutes {} seconds) *** \n".format(Config.run_time, run_time_minutes, run_time_seconds))
    elif Config.run_time <= 60:
        print("\n *** Start collecting raw load cell and raw encoder data for {} seconds *** \n".format(Config.run_time))

    # Set up Analog Task and Counter Task to acquire Analog Data & Counter Data from respective DAQ's
    with nidaqmx.Task() as analog_task, nidaqmx.Task() as counter_task:  # , Connection.open_serial_port("COM9") as connection:
        # Initialize each channel in Analog DAQ module
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_0, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_1, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_2, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_3, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_4, max_val=0.1, min_val=-0.1)
        analog_task.ai_channels.add_ai_voltage_chan(cDAQ9185_ai9252_module_ai_5, max_val=0.1, min_val=-0.1)

        # Initialize each channel in Counter DAQ module
        counter_task.ci_channels.add_ci_ang_encoder_chan(counter='cDAQ9185-1DAC5EBMod1/ctr0', decoding_type=EncoderType.X_4, units=nidaqmx.constants.AngleUnits.DEGREES, pulses_per_rev=400)
        counter_task.ci_channels.add_ci_ang_encoder_chan(counter='cDAQ9185-1DAC5EBMod1/ctr1', decoding_type=EncoderType.X_4, units=nidaqmx.constants.AngleUnits.DEGREES, pulses_per_rev=400)
        counter_task.ci_channels.add_ci_ang_encoder_chan(counter='cDAQ9185-1DAC5EBMod1/ctr2', decoding_type=EncoderType.X_4, units=nidaqmx.constants.AngleUnits.DEGREES, pulses_per_rev=400)
        counter_task.ci_channels.add_ci_ang_encoder_chan(counter='cDAQ9185-1DAC5EBMod1/ctr3', decoding_type=EncoderType.X_4, units=nidaqmx.constants.AngleUnits.DEGREES, pulses_per_rev=400)

        # Set up clock and filter for Analog DAQ
        analog_task.timing.cfg_samp_clk_timing(rate=rate, samps_per_chan=number_of_samples_per_channel, sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS)
        counter_task.timing.cfg_samp_clk_timing(rate=rate, samps_per_chan=number_of_samples_per_channel, sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS)
        # analog_task.timing.delay_from_samp_clk_delay_units = 10364
        # analog_task.timing.delay_from_samp_clk_delay(0.0)

        analog_task.start()
        counter_task.start()

        print("Sampling rate = {}, Run time = {}, Samples = {}".format(rate, Config.run_time, number_of_samples_per_channel))
        print("Reading {} samples for both load cell and encoder: ".format(number_of_samples_per_channel))

        """
        # Acquire all samples from both DAQ's via the read() function. Specifying the number of samples in the read() function waits until all samples are collected before continuing, so it's blocking)
        analog_data = analog_task.read(number_of_samples_per_channel) # =200)
        counter_data = counter_task.read(number_of_samples_per_channel) # =15000)
        """

        # print("Collecting analog data")
        # analog_data = analog_task.read(number_of_samples_per_channel, nidaqmx.constants.WAIT_INFINITELY)  # (n, 6)
        # print("Collecting counter data")
        # counter_data = counter_task.read(number_of_samples_per_channel, nidaqmx.constants.WAIT_INFINITELY)  # (n, 4)

        # print("Collecting analog data")
        # analog_data = analog_task.read(number_of_samples_per_channel, nidaqmx.constants.WAIT_INFINITELY)
        # print("Collecting counter data")
        # counter_data = numpy.zeros((4, number_of_samples_per_channel))

        analog_data = []
        counter_data = []

        print("Collecting analog data")
        print("Collecting counter data")

        start_time = time.time()

        force_limit = 20.0  # Newtons
        torque_limit = 50.0  # Newton-mm

        if Config.load_cell_sn == 28637 or Config.load_cell_sn == 35009 or Config.load_cell_sn == 35010 or Config.load_cell_sn == 35011:
            force_limit = 20.0  # Newtons
        elif Config.load_cell_sn == 38399 or Config.load_cell_sn == 38394 or Config.load_cell_sn == 38395 or Config.load_cell_sn == 38396 or Config.load_cell_sn == 38397:
            torque_limit = 248.0  # Newton-mm

        rows, cols = (number_of_samples_per_channel, 4)
        encoder_live = [[0 for i in range(cols)] for j in range(rows)]

        for i in range(number_of_samples_per_channel):
            curr_analog_reading = analog_task.read(timeout=nidaqmx.constants.WAIT_INFINITELY)
            counter_data.append(counter_task.read(timeout=nidaqmx.constants.WAIT_INFINITELY))  # (n,6)
            analog_data.append(curr_analog_reading)  # (n,6)
            curr_analog_reading_with_offset = numpy.asarray(curr_analog_reading - offset)
            curr_converted_forces = convert_analog_to_forces_torques(curr_analog_reading_with_offset, Config.load_cell_sn)  # (6,)

            # encoder_live[i][0], encoder_live[i][1], encoder_live[i][2], encoder_live[i][3] = Zaber_Functions.encoder(com_port=usb_com_port)

            if abs(curr_converted_forces[2]) > force_limit or abs(curr_converted_forces[0]) > force_limit or abs(curr_converted_forces[1]) > force_limit or abs(curr_converted_forces[5]) > torque_limit or abs(curr_converted_forces[3]) > torque_limit or abs(curr_converted_forces[4]) > torque_limit:
                print("\nYou exceeded the force limit of {} Newtons!!! :(".format(force_limit))
                print("The force measured is: Fx={}, Fy={}, Fz={}\n".format(curr_converted_forces[0], curr_converted_forces[1], curr_converted_forces[2]))
                print("\nYou exceeded the torque limit of {} Newton-mm!!! :(".format(torque_limit))
                print("The torque measured is: Tx={}, Ty={}, Tz={}\n".format(curr_converted_forces[3], curr_converted_forces[4], curr_converted_forces[5]))
                Zaber_Functions.stop_all_stages(com_port=Config.usb_com_port)
                os._exit(0)
                # os._exit(1)
                # exit(0) means a clean exit without any errors / problems
                # exit(1) means there was some issue / error / problem and that is why the program is exiting.

        end_time = time.time()

        print("\nRead Complete in {} sec.".format(end_time - start_time))

        # analog_data = numpy.transpose(analog_data)
        # counter_data = numpy.transpose(counter_data)

    print("\n *** Finished collecting raw load cell and raw encoder data *** \n")

    return analog_data, counter_data, encoder_live


def apply_filters_to_raw_analog_data(analog_data, rate, filter_type=None, cutoff=None, running_mean_window_size=50):

    analog_data_filtered = []

    if filter_type == Constants.lowPass_filter:
        analog_data_filtered = lowpass_filter(analog_data, cutoff, rate)
    elif filter_type == Constants.Notch_filter:
        analog_data_filtered = notch_filter(analog_data, cutoff, rate)
    else:
        return analog_data

    # if running_mean_window_size != 0:
    #     analog_data_filtered = uniform_filter1d(input=analog_data, size=running_mean_window_size, axis=0)

    return analog_data_filtered


# def make_dir_for_exporting():
#     """
#
#     :return:
#     """
#     date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
#
#     newpath = r'DAQ_Data/{}'.format(date_time)
#     newpath_newest = r'DAQ_Data/Newest'
#     if not os.path.exists(newpath):
#         os.makedirs(newpath)
#     if not os.path.exists(newpath_newest):
#         os.makedirs(newpath_newest)
#
#     return newpath, newpath_newest


def export_to_csv(analog_data_offsets, analog_data_raw, analog_data_filtered, force_torque_values_before_offset, analog_data_filtered_with_offset, force_torque_values_after_offset_final, counter_data, linear_displacement_values_final, encoder_live, date_time, newpath_daq_data, newpath_daq_data_newest):
    """

    :param analog_data_raw:
    :param analog_data_filtered:
    :param analog_data_filtered_with_offset:
    :param forces_torques_real_values:
    :param counter_data:
    :param linear_displacement_real_values:
    :param newpath:
    :return:
    """
    print("\nExporting to csv: "
          "\n - analog data RAW, \n - analog data FILTERED, \n - analog data FILTERED and after OFFSET, \n - analog data converted to FORCES & TORQUES, "
          "\n - encoder data RAW, \n - encoder data converted to LINEAR DISPLACEMENT \n")

    test_system, point_to_test, x, y, z = read_points('point_lists.ini')

    config_parameters = [["Test system", test_system],
                         ["Pose no. OR Arc path no.", point_to_test+1],
                         ["Pose X-coord OR Arc Starting X-coord", x],
                         ["Pose Y-coord OR Arc Starting Y-coord", y],
                         ["Pose Z-coord OR Arc Starting Z-coord", z],

                         ["Load Cell SN", Config.load_cell_sn],
                         ["DAQ cDAQ9185 ai9252 module", cDAQ9185_ai9252_module],
                         ["DAQ sampling freq [samples/sec]", Config.sampling_freq],
                         ["DAQ data collection run time [sec]", Config.run_time],
                         ["DAQ number of samples (sampling freq * run time)", Config.number_of_samples],
                         ["DAQ reuse offsets from previous run?", Config.reuse_offsets],
                         ["DAQ low pass filter used?", Config.use_lowPass_filter],
                         ["DAQ filter cutoff freq [Hz]", Config.filter_cutoff_freq],
                         ["DAQ running mean window size", Config.running_mean_window_size],

                         ["Notes", Config.notes],
                         ["Zaber USB COM port", Config.usb_com_port],
                         ["Zaber commanded motion", Config.action_to_execute],
                         ["Zaber gantry sideways clamp being used?", Config.gantry_sideways_clamp],

                         ["Zaber gantry X move absolute or relative [mm]", Config.gantry_X_mm_steps],
                         ["Zaber gantry Y move absolute or relative [mm]", Config.gantry_Y_mm_steps],
                         ["Zaber gantry Z move absolute or relative [mm]", Config.gantry_Z_mm_steps],
                         ["Zaber gantry move absolute units [mm or stepper counts]", Config.gantry_units],
                         ["Zaber gantry speed absolute or relative [mm/s]", Config.gantry_speed_mm_s],

                         ["Zaber rotary angle move absolute or relative [°]", Config.rotary_angle_deg],
                         ["Zaber rotary speed absolute or relative [°/s]", Config.rotary_speed_deg_s],

                         ["Zaber gantry arc speed [mm/s]", Config.arc_speed_mm_s],
                         ["Zaber gantry arc number of cycles", Config.arc_cycles],

                         ["Zaber gantry sine amplitude [mm]", Config.interaction_amplitude_mm_deg],
                         ["Zaber gantry sine frequency [Hz]", Config.interaction_frequency_hz],
                         ["Zaber gantry sine number of cycles", Config.interaction_cycles],
                         ["Zaber gantry constant velocity motion profile", Config.friction_motion_profile],
                         ["Zaber gantry constant velocity relative distance [mm]", Config.friction_PeakToPeak_distance_mm_deg],
                         ["Zaber gantry constant velocity speed [mm/s]", Config.friction_speed_mm_deg_sec],
                         ["Zaber gantry constant velocity cycles", Config.friction_cycles],

                         ["Zaber rotary sine amplitude [°]", Config.interaction_amplitude_mm_deg],
                         ["Zaber rotary sine frequency [Hz]", Config.interaction_frequency_hz],
                         ["Zaber rotary sine number of cycles", Config.interaction_cycles],
                         ["Zaber rotary constant velocity motion profile", Config.friction_motion_profile],
                         ["Zaber rotary constant velocity relative distance [°]", Config.friction_PeakToPeak_distance_mm_deg],
                         ["Zaber rotary constant velocity speed [°/s]", Config.friction_speed_mm_deg_sec],
                         ["Zaber rotary constant velocity cycles", Config.friction_cycles]]

    config_parameters = pandas.DataFrame(config_parameters)
    config_parameters.to_csv('{}/{}_Config_Parameters.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    config_parameters.to_csv('{}/Newest_Config_Parameters.csv'.format(newpath_daq_data_newest), header=False, index=False)

    analog_data_offsets_csv = pandas.DataFrame(analog_data_offsets)
    # analog_data_offsets_csv = numpy.transpose(analog_data_offsets_csv)
    analog_data_offsets_csv.to_csv('{}/{}_Load_Cell_Taring_Values.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    analog_data_offsets_csv.to_csv('{}/Newest_Load_Cell_Taring_Values.csv'.format(newpath_daq_data_newest), header=False, index=False)

    analog_data_raw_csv = pandas.DataFrame(analog_data_raw)
    # analog_data_raw_csv = numpy.transpose(analog_data_raw_csv)
    analog_data_raw_csv.to_csv('{}/{}_Raw_Analog_Data.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    analog_data_raw_csv.to_csv('{}/Newest_Raw_Analog_Data.csv'.format(newpath_daq_data_newest), header=False, index=False)

    analog_data_filtered_csv = pandas.DataFrame(analog_data_filtered)
    # analog_data_filtered_csv = numpy.transpose(analog_data_filtered_csv)
    analog_data_filtered_csv.to_csv('{}/{}_Analog_Data_Filtered.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    analog_data_filtered_csv.to_csv('{}/Newest_Analog_Data_Filtered.csv'.format(newpath_daq_data_newest), header=False, index=False)

    force_torque_values_before_offset_csv = pandas.DataFrame(force_torque_values_before_offset)
    # force_torque_values_before_offset_csv = numpy.transpose(force_torque_values_before_offset_csv)
    force_torque_values_before_offset_csv.to_csv('{}/{}_Converted_To_Forces_Torques_Before_Offset.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    force_torque_values_before_offset_csv.to_csv('{}/Newest_Converted_To_Forces_Torques_Before_Offset.csv'.format(newpath_daq_data_newest), header=False, index=False)

    analog_data_filtered_with_offset_csv = pandas.DataFrame(analog_data_filtered_with_offset)
    # analog_data_filtered_with_offset_csv = numpy.transpose(analog_data_filtered_with_offset_csv)
    analog_data_filtered_with_offset_csv.to_csv('{}/{}_Analog_Data_Filtered_With_Offset.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    analog_data_filtered_with_offset_csv.to_csv('{}/Newest_Analog_Data_Filtered_With_Offset.csv'.format(newpath_daq_data_newest), header=False, index=False)

    forces_torques_real_values_csv = pandas.DataFrame(force_torque_values_after_offset_final)
    # forces_torques_real_values_csv = numpy.transpose(forces_torques_real_values_csv)
    forces_torques_real_values_csv.to_csv('{}/{}_Converted_To_Forces_Torques.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    forces_torques_real_values_csv.to_csv('{}/Newest_Converted_To_Forces_Torques.csv'.format(newpath_daq_data_newest), header=False, index=False)

    counter_data_csv = pandas.DataFrame(counter_data)
    # counter_data_csv = numpy.transpose(counter_data_csv)
    counter_data_csv.to_csv('{}/{}_Raw_Encoder_Data.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    counter_data_csv.to_csv('{}/Newest_Raw_Encoder_Data.csv'.format(newpath_daq_data_newest), header=False, index=False)

    linear_displacement_real_values_csv = pandas.DataFrame(linear_displacement_values_final)
    # linear_displacement_real_values_csv = numpy.transpose(linear_displacement_real_values_csv)
    linear_displacement_real_values_csv.to_csv('{}/{}_Converted_To_Linear_Displacement.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    linear_displacement_real_values_csv.to_csv('{}/Newest_Converted_To_Linear_Displacement.csv'.format(newpath_daq_data_newest), header=False, index=False)

    encoder_live_csv = pandas.DataFrame(encoder_live)
    # encoder_live_csv = numpy.transpose(encoder_live_csv)
    encoder_live_csv.to_csv('{}/{}_Encoder_Live.csv'.format(newpath_daq_data, date_time), header=False, index=False)
    encoder_live_csv.to_csv('{}/Newest_Encoder_Live.csv'.format(newpath_daq_data_newest), header=False, index=False)

    if Config.action_to_execute == 'gantry_interaction_force':
        print("\nExported to csv the data for:"
              "\n - Points Set = '{}', \n - Pose/Point #{}, \n - Test Type = '{}'"
              "\nWith Zaber parameters:"
              "\n - amplitude = {} mm, \n - frequency = {} Hz, \n - cycles = {} cycles \n"
              .format(test_system, point_to_test+1, Config.action_to_execute, Config.interaction_amplitude_mm_deg, Config.interaction_frequency_hz, Config.interaction_cycles))
    elif Config.action_to_execute == 'gantry_force_to_start':
        print("\nExported to csv the data for:"
              "\n - Points Set = '{}', \n - Pose/Point #{}, \n - Test Type = '{}'"
              "\nWith Zaber parameters:"
              "\n - peak-to-peak displacement = {} mm, \n - speed = {} mm/s, \n - cycles = {} cycles \n"
              .format(test_system, point_to_test+1, Config.action_to_execute, Config.friction_PeakToPeak_distance_mm_deg, Config.friction_speed_mm_deg_sec, Config.friction_cycles))
    elif Config.action_to_execute == 'rotary_interaction_torque':
        print("\nExported to csv the data for:"
              "\n - Points Set = '{}', \n - Pose/Point #{}, \n - Test Type = '{}'"
              "\nWith Zaber parameters:"
              "\n - amplitude = {} °, \n - frequency = {} Hz, \n - cycles = {} cycles \n"
              .format(test_system, point_to_test+1, Config.action_to_execute, Config.interaction_amplitude_mm_deg, Config.interaction_frequency_hz, Config.interaction_cycles))
    elif Config.action_to_execute == 'rotary_torque_to_start':
        print("\nExported to csv the data for:"
              "\n - Points Set = '{}', \n - Pose/Point #{}, \n - Test Type = '{}'"
              "\nWith Zaber parameters:"
              "\n - peak-to-peak displacement = {} °, \n - speed = {} °/s, \n - cycles = {} cycles \n"
              .format(test_system, point_to_test+1, Config.action_to_execute, Config.friction_PeakToPeak_distance_mm_deg, Config.friction_speed_mm_deg_sec, Config.friction_cycles))
    # elif Config.action_to_execute == 'gantry_trace_arc_abs_origin' or Config.action_to_execute == 'gantry_trace_arc_rel_origin':
    else:
        print("\nExported to csv")

    newpath_daq_data = newpath_daq_data.replace('/', "\\")

    print("Filepath and Filename: "
          "\n{}\\{}"
          "\n{}\n\n"
          .format(os.getcwd(), newpath_daq_data, date_time))


def export_to_xlsx(analog_data, analog_data_with_offset, forces_torques_real_values, counter_data, linear_displacement_real_values):
    """
    Export raw analog data, analog data converted to forces & torques, and raw counter data to xlsx
    :parameter analog_data: raw analog data
    :parameter analog_data_with_offset:
    :parameter forces_torques_real_values: analog data converted to forces & torques
    :parameter counter_data: raw counter data
    :parameter linear_displacement_real_values: counter data (angles) converted to linear displacement
    :return: N/A
    """
    print("\n *** Exporting to xlsx: Raw Analog data, Analog Data after Offset, Analog Data converted to Forces & Torques, Raw Encoder data, and Encoder Data converted to Linear Displacement. ***")

    date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    workbook = xlsxwriter.Workbook("DAQ_Data/{}.xlsx".format(date_time))
    workbook_copy = xlsxwriter.Workbook("DAQ_Data/newest.xlsx")

    worksheet1 = workbook.add_worksheet('Raw Analog Data')
    worksheet2 = workbook.add_worksheet('Analog Data after Offset')
    worksheet3 = workbook.add_worksheet('Converted to Forces and Torques')
    worksheet4 = workbook.add_worksheet('Raw Encoder Data')
    worksheet5 = workbook.add_worksheet('Converted to Lin Displacement')
    worksheet1_copy = workbook_copy.add_worksheet('Raw Analog Data')
    worksheet2_copy = workbook_copy.add_worksheet('Analog Data after Offset')
    worksheet3_copy = workbook_copy.add_worksheet('Converted to Forces and Torques')
    worksheet4_copy = workbook_copy.add_worksheet('Raw Encoder Data')
    worksheet5_copy = workbook_copy.add_worksheet('Converted to Lin Displacement')

    # With enumerate(), the function returns two loop variables:
    # 1) The count of the current iteration
    # 2) The value of the item at the current iteration
    # https://realpython.com/python-enumerate/
    row = 0
    for col, data in enumerate(analog_data):
        worksheet1.write_column(row, col, data)
        worksheet1_copy.write_column(row, col, data)
    for col, data in enumerate(analog_data_with_offset):
        worksheet2.write_column(row, col, data)
        worksheet2_copy.write_column(row, col, data)
    for col, data in enumerate(forces_torques_real_values):
        worksheet3.write_column(row, col, data)
        worksheet3_copy.write_column(row, col, data)
    for col, data in enumerate(counter_data):
        worksheet4.write_column(row, col, data)
        worksheet4_copy.write_column(row, col, data)
    for col, data in enumerate(linear_displacement_real_values):
        worksheet5.write_column(row, col, data)
        worksheet5_copy.write_column(row, col, data)

    workbook.close()
    workbook_copy.close()

    print("*** Exported to xlsx: Raw Analog data, Analog Data after Offset, Analog Data converted to Forces & Torques, Raw Encoder data, and Encoder Data converted to Linear Displacement. ***\n")



def acquire_data(Config, offsets, date_time, newpath_daq_data, newpath_daq_data_newest):
    load_cell_data_raw, encoder_data_raw, encoder_live = collect_analog_counter_readings(rate=Config.sampling_freq, number_of_samples_per_channel=Config.number_of_samples, offset=offsets)
    load_cell_data_filtered = apply_filters_to_raw_analog_data(analog_data=load_cell_data_raw, rate=Config.sampling_freq, filter_type=Config.filter, cutoff=Config.filter_cutoff_freq, running_mean_window_size=Config.running_mean_window_size)
    force_torque_values_before_offset = convert_analog_to_forces_torques(analog_data=load_cell_data_raw, load_cell_sn=Config.load_cell_sn)
    # print(load_cell_data_filtered.shape, offsets.shape)
    load_cell_data_filtered_with_offset = load_cell_data_filtered - offsets
    force_torque_values_after_offset_final = convert_analog_to_forces_torques(analog_data=load_cell_data_filtered_with_offset, load_cell_sn=Config.load_cell_sn)
    print("\n *** Completed conversion from load cell raw analog voltage readings to forces and torques *** \n")
    lin_displacement_values_final = convert_counter_to_lin_displacement(counter_data=encoder_data_raw)
    print("\n *** Completed conversion from encoder raw angle readings to linear displacement *** \n")
    # date_time, newpath, newpath_newest, newpath_zaber_log, newpath_zaber_log_pos = make_dir_for_exporting()
    export_to_csv(offsets, load_cell_data_raw, load_cell_data_filtered, force_torque_values_before_offset, load_cell_data_filtered_with_offset, force_torque_values_after_offset_final, encoder_data_raw, lin_displacement_values_final, encoder_live, date_time, newpath_daq_data, newpath_daq_data_newest)
    # export_to_xlsx(load_cell_data_raw, load_cell_data_with_offset, force_torque_values, encoder_data_raw, lin_displacement_values)


if __name__ == '__main__':

    ' Data Acquisition parameters '
    sampling_freq = 2500  # Hz = samples/sec
    run_time = 10  # sec (duration of reading load cell and encoders)
    number_of_samples = sampling_freq * run_time
    ' Filter parameters, Running Mean parameter '
    filter_cutoff_freq = 200  # Hz (noise is 62.5 Hz)
    manual_filter = Constants.lowPass_filter
    # manual_filter = DAQ_constants.notch_filter
    # manual_filter = None
    running_mean_window_size = 10  # 0 means not using running mean

    ' Connect to DAQ '
    connect_to_daq()
    ' Obtain load cell offset values '
    offset_values = get_load_cell_offsets(Config)
    # offset_values = offset_values.reshape(-1, 1)

    date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    ''' Directory for DAQ_Data '''
    newpath_daq_data = r'DAQ_Data/{}'.format(date_time)
    newpath_daq_data_newest = r'DAQ_Data/Newest'
    if not os.path.exists(newpath_daq_data):
        os.makedirs(newpath_daq_data)
    if not os.path.exists(newpath_daq_data_newest):
        os.makedirs(newpath_daq_data_newest)

    acquire_data(Config, offset_values, date_time, newpath_daq_data, newpath_daq_data_newest)
import csv
import numpy
import os

class DAQ_Offsets:
    def __init__(self):
        self.values = numpy.zeros(6)
        self.file_name = "offset_file.csv"

    def read_offset_file(self):
        """
        read offset file and return offset file
        """
        isExist = os.path.exists(self.file_name)
        if not isExist:
            print("The offset file does not exist!")
            return False
        with open(self.file_name) as csv_file:
            self.values = numpy.array(list(csv.reader(csv_file, delimiter=','))).astype("float")
            self.values = self.values.flatten()
            print("Successful readings of offsets")
        return True

    def save_offset_file(self, offset):
        """
        Write offset file in a csv
        """
        try:
            self.values = offset
            numpy.savetxt(self.file_name, offset, delimiter=",")
            print("Successful writings of offsets")
            return True
        except:
            print("Cannot write offsets to the file")
            return False
